<div wire:ignore >
    <label class="col-sm-2 control-label" for=" ">Content </label>
    <div class="col-sm-10" >
        <div class=" border border-gray-600 " >
            <textarea
                wire:ignore
                wire:key="editor-{{ now() }}"
                class="w-full" wire:model.lazy="content" name="content" id="content"></textarea>
        </div>
        <div>
            @error('content') <span class="text-red-500 text-xs">{{ $message }}</span> @enderror
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
    <script>
        const editor = CKEDITOR.replace('content', {
        });
        editor.on('change', function(event) {
            @this.set('content', event.editor.getData());
        })

        CKEDITOR.config.allowedContent = true;
        CKEDITOR.filebrowserUploadMethod = 'form';
        CKEDITOR.editorConfig = function(config) {
            config.extraPlugins = 'colorbutton,colordialog,panelbutton';
        };

        CKEDITOR.on('dialogDefinition', function(ev) {
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;

            if (dialogName == 'image') {
                var infoTab2 = dialogDefinition.getContents('advanced');
                dialogDefinition.removeContents('advanced');
                var infoTab = dialogDefinition.getContents('info');
                infoTab.remove('txtBorder');
                infoTab.remove('cmbAlign');
                infoTab.remove('txtWidth');
                infoTab.remove('txtHeight');
                infoTab.remove('txtCellSpace');
                infoTab.remove('txtCellPad');
                infoTab.remove('txtCaption');
                infoTab.remove('txtSummary');
            }
        });

    </script>
</div>
