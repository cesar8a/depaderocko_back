@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        {{-- @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="#" wire:click="previousPage" wire:loading.attr="disabled" rel="prev"
                    aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif --}}

        <?php
        $start = $paginator->currentPage() - 2; // show 3 pagination links before current
        $end = $paginator->currentPage() + 2; // show 3 pagination links after current
        if ($start < 1) {
            $start = 1; // reset start to 1
            $end += 1;
        }
        if ($end >= $paginator->lastPage()) {
            $end = $paginator->lastPage();
        } // reset end to last page
        ?>

        @if ($start > 1)
            <li class="page-item">
                <a class="page-link" href="#" wire:click="gotoPage({{ 1 }}, 'page')"
                    wire:loading.attr="disabled">{{ 1 }}</a>
            </li>
            @if ($paginator->currentPage() != 4)
                {{-- "Three Dots" Separator --}}
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
            @endif
        @endif
        @for ($i = $start; $i <= $end; $i++)
            <li class="page-item {{ $paginator->currentPage() == $i ? ' active' : '' }}">
                <a class="page-link" href="#" wire:click="gotoPage({{ $i }}, 'page')"
                    wire:loading.attr="disabled">{{ $i }}</a>
            </li>
        @endfor
        @if ($end < $paginator->lastPage())
            @if ($paginator->currentPage() + 3 != $paginator->lastPage())
                {{-- "Three Dots" Separator --}}
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
            @endif
            <li class="page-item">
                <a class="page-link" href="#" wire:click="gotoPage({{ $paginator->lastPage() }}, 'page')"
                    wire:loading.attr="disabled">{{ $paginator->lastPage() }}</a>
            </li>
        @endif

        {{-- Next Page Link --}}
        {{-- @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="#" wire:click="nextPage" wire:loading.attr="disabled" rel="next"
                    aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>
        @endif --}}
    </ul>
@endif
