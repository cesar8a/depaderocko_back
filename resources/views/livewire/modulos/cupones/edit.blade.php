<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <div class="card no-b  no-r">
                <div class="card-body">
                    <h5 class="card-title">Editar Cupón</h5>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">Título</label>
                                <input type="text" wire:model='titulo' class="form-control r-0 light s-12">
                            </div>
                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">Código</label>
                                <input type="text" wire:model='codigo' disabled class="form-control r-0 light s-12" oninput="this.value = this.value.toUpperCase()">
                            </div>
                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">RockoCoins</label>
                                <input type="text" wire:model='rockocoins' class="form-control r-0 light s-12">
                            </div>

                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">Descripción</label>
                                <textarea name="" wire:model='descripcion' class="form-control r-0 light s-12" id="" cols="30"
                                    rows="5"></textarea>
                            </div>


                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">Fecha inicio</label>
                                <input type="datetime-local" wire:model='fec_inicio' class="form-control r-0 light s-12">
                            </div>


                            <div class="form-group m-0">
                                <label for="name" class="col-form-label s-12">Fecha fin</label>
                                <input type="datetime-local" wire:model='fec_fin' class="form-control r-0 light s-12">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <button type="submit" wire:click='updateCupon()' class="btn w-100" style="background-color: #F4C1E1;color:#fff"><i
                            class="icon-save mr-2"></i>Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
