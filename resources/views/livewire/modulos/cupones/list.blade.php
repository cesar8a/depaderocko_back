<div class="container">
    <div class="row my-3 ml-3 mr-3">
        <div class="col-md-12">
            {{-- <div class="input-group px-6 py-4">
                <div class="form-outline">
                    <input class="form-control" type="text" wire:model.debounce.300ms="search"
                        placeholder="Buscar cliente">
                </div>
            </div> --}}
            <div class="card r-0 shadow">
                <div class="table-responsive">
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>Título</th>
                                <th>Código</th>
                                <th>RockoCoins</th>
                                <th>Descripción</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th>Contador</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($cupones as $cupon)
                                <tr>
                                    <td>{{ $cupon->titulo }}</td>
                                    <td>{{ $cupon->codigo }}</td>
                                    <td>{{ $cupon->rockocoins }}</td>
                                    <td>{{ $cupon->descripcion }}</td>
                                    <td>{{ $cupon->fec_inicio }}</td>
                                    <td>{{ $cupon->fec_fin }}</td>
                                    <td>{{ $cupon->count }}</td>
                                    <td class="text-center">
                                        <i class="s-24 icon-pencil-square" style="font-size: 30px; color:#F4C1E1"
                                            wire:click='edit({{ $cupon->id }})'></i>
                                    </td>
                                    <td class="text-center">
                                        <i class="s-24 icon-trash text-secondary"
                                            wire:click='eliminar({{ $cupon->id }})' style="font-size: 30px"></i>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="9" class="py-3 italic">No hay información</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-end">
                    {{-- {{ $clientes->links('vendor.pagination.bootstrap-4') }} --}}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="modalEliminarCupon" tabindex="-1" role="dialog"
        aria-labelledby="modalEliminarCupon" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar el cupón?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                    <button type="button" wire:click.prevent="eliminarCupon" wire:loading.attr="disabled"
                        class="btn close-modal" style="background-color: #F4C1E1;color:#fff">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('openModalEliminarCupon', event => {
        $("#modalEliminarCupon").modal('show');
    })

    window.addEventListener('closeModalEliminarCupon', event => {
        $("#modalEliminarCupon").modal('hide');
    })
</script>