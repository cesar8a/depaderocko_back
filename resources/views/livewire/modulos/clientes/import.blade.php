<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form wire:submit.prevent="enviarInvitacion" accept-charset="UTF-8" enctype="multipart/form-data"
                wire:loading.attr="disabled">
                @csrf
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Adjuntar lista</h5>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group mt-3">
                                    <label for="name" class="col-form-label s-12">Excel</label>
                                    <input class="form-control" type="file" name="file" wire:model="file"
                                        id="upload{{ $iteration }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="submit" class="btn w-100" wire:loading.attr="disabled" style="background-color: #F4C1E1;color:#fff"><i
                                class="icon-save mr-2"></i>Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
