<div class="container">
    <div class="row my-3 ml-1 mr-1">
        <div class="col-md-12">
            <div class="input-group px-6 py-4">
                <div class="form-outline">
                    <input class="form-control" type="text" wire:model.debounce.300ms="search"
                        placeholder="Buscar RockoFriend">
                </div>
            </div>
            <div class="card r-0 shadow">
                <div class="table-responsive">
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>RockoCard</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Documento</th>
                                <th>Celular</th>
                                <th>RockoCoins</th>
                                <th>Canjear RockoCoins</th>
                                <th>Agregar RockoCoins</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($clientes as $cliente)
                                <tr>
                                    <td>{{ $cliente->rockocard }}</td>
                                    <td>{{ $cliente->nombre }}</td>
                                    <td>{{ $cliente->apellido }}</td>
                                    <td>{{ $cliente->documento }}</td>

                                    <td>{{ $cliente->celular }}</td>
                                    <td class="text-center">{{ $cliente->rockocoins }}</td>
                                    <td class="text-center">
                                        <button class="btn" type="button"
                                            wire:click='modalUsarRockocoins({{ $cliente->id }})'>
                                            <i class="s-24 icon-minus" style="font-size: 30px; color: #F4C1E1!important"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn" type="button"
                                            wire:click='modalAgregarRockocoins({{ $cliente->id }})'>
                                            <i class="s-24 icon-gift" style="font-size: 30px; color: #F4C1E1!important"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="9" class="py-3 italic">No hay información</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-end">
                    {{ $clientes->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="modalUsarRockocoins" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" >Usar RockoCoins</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label" >Cantidad:</label>
                        <input wire:ignore	type="text" class="form-control" wire:model.defer="cantidad">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descripción:</label>
                        <textarea wire:ignore class="form-control" wire:model.defer="descripcion"></textarea>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                    <button type="button" wire:click.prevent="usarRockoCoins" wire:loading.attr="disabled"
                        class="btn close-modal" style="background-color: #F4C1E1;color:#fff">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="modalAgregarRockocoins" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" >Agregar RockoCoins</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label" >Cantidad:</label>
                        <input wire:ignore	type="text" class="form-control" wire:model.defer="cantidad">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descripción:</label>
                        <textarea wire:ignore class="form-control" wire:model.defer="descripcion"></textarea>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                    <button type="button" wire:click.prevent="agregarRockocoins" wire:loading.attr="disabled"
                        class="btn close-modal" style="background-color: #F4C1E1;color:#fff">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('openModalUsarRockocoins', event => {
        $("#modalUsarRockocoins").modal('show');
    })

    window.addEventListener('closeModalUsarRockocoins', event => {
        $("#modalUsarRockocoins").modal('hide');
    })
    window.addEventListener('openModalAgregarRockocoins', event => {
        $("#modalAgregarRockocoins").modal('show');
    })

    window.addEventListener('closeModalAgregarRockocoins', event => {
        $("#modalAgregarRockocoins").modal('hide');
    })
</script>
