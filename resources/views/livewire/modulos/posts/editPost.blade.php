@extends('layouts.app')

@section('contenido')
    <div>
        <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
            <div class="container-fluid text-white" style="background-color: #F4C1E1">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-box"></i>
                            RockoBlog
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a href="{{ route('posts') }}" class="nav-link"><i class="icon icon-user-plus"></i>RockoBlogs</a>
                        </li>

                        <li>
                            <a href="{{ route('createPost') }}" class="nav-link"><i
                                    class="icon icon-user-plus"></i>Nuevo RockoBlog</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row my-3">
            <div class="col-md-10  offset-md-1">
                <form method="POST" action="{{route('updatePost')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <div class="card no-b  no-r">
                        <div class="card-body">
                            <h5 class="card-title">Crear RockoBlog</h5>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Estado</label>
                                    <select name="estado" class="form-control r-0 light s-12" id="">
                                        <option value="">SELECCIÓN</option>
                                        <option value="1" @if($post->estado == 1) selected @endif>PUBLICADO</option>
                                        <option value="2" @if($post->estado == 2) selected @endif>NO PUBLICADO</option>
                                        <option value="3" @if($post->estado == 3) selected @endif>BORRADOR</option>
                                    </select>
                                </div>

                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">CategorÍa</label>
                                        <select name="idCategoria" class="form-control r-0 light s-12" wire:model='idCategoria' id="">
                                            <option value="">SELECCIÓN</option>
                                            @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->id }}" @if($categoria->id == $post->idCategoria) selected @endif>{{ $categoria->categoria }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Url amigable:</label>
                                    <input type="text" name="slug" value="{{$post->slug}}" class="form-control r-0 light s-12">
                                </div>

                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Título</label>
                                        <input type="text" name="titulo" value="{{$post->titulo}}" class="form-control r-0 light s-12">
                                        <input type="hidden" name="idPost" value="{{$post->id}}">
                                    </div>

                                    <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fecha</label>
                                    <input type="date" name="fecha" value="{{$fecha}}" class="form-control r-0 light s-12">
                                </div>

                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Resumen</label>
                                        <textarea  name="resumen" class="form-control r-0 light s-12" value="" cols="30" rows="5">{{$post->resumen}}</textarea>
                                    </div>


                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Descripción</label>
                                        <textarea name="descripcion" class="form-control required" id="editor" cols="10">{!! $post->contenido !!}</textarea>
                                    </div>


                                    <div class="form-group mt-3">
                                        <label for="name" class="col-form-label s-12">Imagen</label>
                                        <input type="file" name="imagen" class="form-control r-0 light s-12">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <button type="submit" class="btn w-100" style="background-color: #F4C1E1;color:#fff"><i
                                    class="icon-save mr-2"></i>Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/ckeditor5/34.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
