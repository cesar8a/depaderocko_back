<div class="row my-3 ml-3 mr-3">
    <div class="col-md-12">
        <div class="card r-0 shadow">
            <div class="table-responsive">
                <form>
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>Id</th>
                                <th>Categoría</th>
                                <th>Autor</th>
                                <th>Título</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>{{ $post->categoria->categoria }}</td>
                                    <td>{{ $post->usuario->name }}</td>
                                    <td>{{ $post->titulo }}</td>
                                    <td>
                                        @switch($post->estado)
                                            @case(1)
                                                <span class="badge " style="font-size: 12px; background-color: #F4C1E1; color: #fff;"> Publicado </span>
                                            @break

                                            @case(2)
                                                <span class="badge badge-secondary" style="font-size: 12px;"> No Publicado</span>
                                            @break

                                            @case(3)
                                                <span class="badge badge-warning" style="font-size: 12px;"> Borrador</span>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <a href="{{route('editPost', "$post->slug")}}"><i class="s-24 icon-pencil-square" style="font-size: 30px; color:#F4C1E1"></i></a>
                                        <i class="s-24 icon-trash text-secondary"
                                            wire:click='eliminar({{ $post->id }})' style="font-size: 30px"></i>
                                    </td>
                                </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="3" class="py-3 italic">No hay información</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>


    </div>
