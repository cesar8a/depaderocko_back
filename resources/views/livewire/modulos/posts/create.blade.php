<div class="container-fluid">

    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form wire:submit.prevent="save">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Crear Post</h5>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Categoría</label>
                                    <select name="" class="form-control r-0 light s-12" wire:model='idCategoria' id="">
                                        <option value="">SELECCIÓN</option>
                                        @foreach ($categorias as $categoria)
                                            <option value="{{ $categoria->id }}">{{ $categoria->categoria }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">TÍtulo</label>
                                    <input type="text" wire:model='titulo' class="form-control r-0 light s-12">
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Resumen</label>
                                    <textarea name="" wire:model='resumen' class="form-control r-0 light s-12" id="" cols="30" rows="5"></textarea>
                                </div>


                                {{-- <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Descripción</label>
                                     <textarea wire:ignore wire:model='descripcion' class="form-control r-0 light s-12" id="editor" cols="30" rows="5"></textarea>
                                    <textarea wire:model="message" class="form-control required" wire:ignore name="message" id="message"></textarea>
                                </div> --}}

                                <div wire:ignore>
                                    <div id="yourCkId"></div>
                                </div>
                                <textarea id="yourTextAreaId" wire:key="uniqueKey" wire:model="description" style="display: none">  </textarea>

                                <div class="form-group mt-3">
                                    <label for="name" class="col-form-label s-12">Imagen</label>
                                    <input type="file" wire:model="imagen" class="form-control r-0 light s-12">
                                    @if ($imagen)
                                        <label for="name" class="col-form-label s-12">Previsualizar:</label>
                                        <img src="{{ $imagen->temporaryUrl() }}" class="w-100">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="submit" wire:click='save()' class="btn btn-primary w-100"><i
                                class="icon-save mr-2"></i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
