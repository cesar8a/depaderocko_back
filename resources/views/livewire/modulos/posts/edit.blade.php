<div class="container-fluid">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css" />
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form wire:submit.prevent="update">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Editar Post</h5>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Estado</label>
                                    <select name="" class="form-control r-0 light s-12" wire:model='estado' id="">
                                        <option value="">SELECCIÓN</option>
                                        <option value="1">PUBLICADO</option>
                                        <option value="2">NO PUBLICADO</option>
                                        <option value="3">BORRADOR</option>
                                    </select>
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Categoría</label>
                                    <select name="" class="form-control r-0 light s-12" wire:model='idCategoria' id="">
                                        <option value="">SELECCIÓN</option>
                                        @foreach ($categorias as $categoria)
                                            <option value="{{ $categoria->id }}">{{ $categoria->categoria }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Url amigable:</label>
                                    <input type="text" wire:model='slug' class="form-control r-0 light s-12">
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Título</label>
                                    <input type="text" wire:model='titulo' class="form-control r-0 light s-12">
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fecha</label>
                                    <input type="date" wire:model='fecha' class="form-control r-0 light s-12">
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Resumen</label>
                                    <textarea name="" wire:model='resumen' class="form-control r-0 light s-12" id="" cols="30" rows="5"></textarea>
                                </div>


                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Descripción</label>
                                    {{-- <textarea name="" class="form-control r-0 light s-12" id="" cols="30" rows="5"></textarea> --}}
                                    {{-- <input id="x" type="hidden" wire:model='descripcion' name="content">
                                    <trix-editor input="x"></trix-editor> --}}
                                    <textarea name="" wire:model='descripcion' class="form-control r-0 light s-12" id="" cols="30" rows="5"></textarea>
                                </div>

                                <div class="form-group mt-3">
                                    <label for="name" class="col-form-label s-12">Imagen</label>
                                    <input type="file" wire:model="imagen" class="form-control r-0 light s-12">
                                    @if ($imagen)
                                        <label for="name" class="col-form-label s-12">Previsualizar:</label>
                                        <img src="{{ $imagen->temporaryUrl() }}" class="w-100">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary w-100"><i
                                class="icon-save mr-2"></i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.js"></script>
</div>
