<style>
    .dropdown-toggle::after {
        display: none;
    }
</style>

<div class="container" style="max-width:100%" wire:once>

    <div class="row my-3 ml-3 mr-3">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body" style="font-size: 1.4rem;">
                            RockoFriends: {{ $cantidadRockofriends }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body" style="font-size: 1.4rem;">
                            Mascotas: {{ $cantidadMascotas }}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="row ml-3 mr-3">
                            <div class="col-md-12 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf1">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3">
                            <div class="col-md-6 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf3">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf5">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3">
                            <div class="col-md-6 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf4">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3">
                            <div class="col-md-12 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf6">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3">
                            <div class="col-md-12 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf8">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3">
                            <div class="col-md-12 my-3">
                                <div class="card r-0 shadow">
                                    <div id="graf9">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade bd-example-modal-lg" id="modalDistritos" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12 my-3">
                            <div class="card r-0 shadow">
                                <div id="graf7">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            loading: 'Cargando...',
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre',
                'Octubre', 'Noviembre', 'Diciembre'
            ],
            weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            exportButtonTitle: "Exportar",
            printButtonTitle: "Importar",
            rangeSelectorFrom: "Desde",
            rangeSelectorTo: "Hasta",
            rangeSelectorZoom: "Período",
            downloadPNG: 'Descargar imagen PNG',
            downloadJPEG: 'Descargar imagen JPEG',
            downloadPDF: 'Descargar imagen PDF',
            downloadSVG: 'Descargar imagen SVG',
            printChart: 'Imprimir',
            resetZoom: 'Reiniciar zoom',
            resetZoomTitle: 'Reiniciar zoom',
            thousandsSep: ",",
            decimalPoint: '.',
            viewData: 'Ver tabla de datos',
            hideData: 'Ocultar tabla de datos',
            viewFullscreen: 'Ver en pantalla completa',
            exitFullscreen: 'Salir de pantalla completa',
            downloadCSV: 'Descargar CSV',
            downloadXLS: 'Descargar XLS'
        }
    });

    var colors = ["#F4C1E1", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#7cb5ec", "#e4d354", "#2b908f", "#f45b5b",
        "#91e8e1"
    ];

    function stackedAreaChart(cat, series,height) {
        return Highcharts.chart('graf1', {
            credits: {
                enabled: false
            },
            colors: colors,
            chart: {
                type: 'area',
                height: height
            },
            title: {
                text: 'Cantidad de los RockoFriends registrados por día'
            },
            /*
            subtitle: {
                text: 'Source: Wikipedia.org'
            },*/
            xAxis: {
                categories: cat,
                tickmarkPlacement: 'on',
                title: {
                    enabled: true,
                    text: 'Días'

                }
            },
            yAxis: {
                title: {
                    text: 'Usuarios Registrados'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                split: true,
                valueSuffix: ' personas'
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#F4C1E1',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    },
                    dataLabels: {
                        enabled: true
                    }
                },
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [series],
            legend: {
                enabled: false
            }
        });
    }

    function pieChart(container, data, tituloGraf, nombreSeries, height ) {
        return Highcharts.chart(container, {
            credits: {
                enabled: false
            },
            colors: colors,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height: height
            },
            title: {
                text: tituloGraf
            },
            tooltip: {
                pointFormat: '{series.name}: <b> {point.y} ({point.percentage:.1f}%)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)'
                    }
                }
            },
            series: [{
                name: nombreSeries,
                colorByPoint: true,
                data: data
            }]
        });
    }

    function barColumnChart(container, categories, series, tituloGraf, height, typeChart) {
        return Highcharts.chart(container, {
            credits: {
                enabled: false
            },
            colors: colors,
            chart: {
                type: typeChart,
                height: height
            },
            title: {
                text: tituloGraf
            },
            tooltip: {
                formatter:function() {
                    var pcnt = (this.y / this.series.data.map(p => p.y).reduce((a, b) => a + b, 0)) * 100;
                    return this.y+ ' ('+Highcharts.numberFormat(pcnt) + '%)';
                }
            },
            /*
            subtitle: {
                text: 'Source: <a ' +
                    'href="https://en.wikipedia.org/wiki/List_of_continents_and_continental_subregions_by_population"' +
                    'target="_blank">Wikipedia.org</a>'
            },*/
            xAxis: {
                categories: categories,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Personas registradas',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    cursor: 'pointer',
                    events: {
                        click: function(event, e) {}
                    },
                    point: {
                        events: {
                            click: function() {
                                if (typeof this.series.chart.options.series[this.colorIndex] !==
                                    'undefined' && typeof this.series.chart.options.series[this.colorIndex]
                                    .column_key !== 'undefined') {
                                    var column_index = this.series.chart.options.series[this.colorIndex]
                                        .column_key[this.index];
                                    tituloDistritos = 'Según Distritos de residencia (' + this.category +
                                        ')';
                                    window.livewire.emit('filtrarPorDistritos', {
                                        id_departamento: column_index
                                    });
                                }
                            }
                        }
                    },
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            var pcnt = (this.y / this.series.data.map(p => p.y).reduce((a, b) => a + b, 0)) * 100;
                            return this.y+ ' ('+Highcharts.numberFormat(pcnt) + '%)';
                        }
                    }
                }

            },
            series: series
        });

    }

    var categoriesAreaChart = {!! json_encode($categoriesAreaChart) !!};
    var seriesAreaChart = {!! json_encode($seriesAreaChart) !!};
    var dataGeneros = {!! json_encode($dataGeneros) !!};
    var dataEngreidos = {!! json_encode($dataEngreidos) !!};
    var categoriesPaisNacimiento = {!! json_encode($categoriesPaisNacimiento) !!};
    var seriesPaisNacimiento = {!! json_encode($seriesPaisNacimiento) !!};
    var categoriesPaisResidencia = {!! json_encode($categoriesPaisResidencia) !!};
    var seriesPaisResidencia = {!! json_encode($seriesPaisResidencia) !!};
    var categoriesDepartamentos = {!! json_encode($categoriesDepartamentos) !!};
    var seriesDepartamentos = {!! json_encode($seriesDepartamentos) !!};
    var categoriesProfesiones = {!! json_encode($categoriesProfesiones) !!};
    var seriesProfesiones = {!! json_encode($seriesProfesiones) !!};
    var categoriesEdades = {!! json_encode($categoriesEdades) !!};
    var seriesEdades = {!! json_encode($seriesEdades) !!};
    function draw(){
        stackedAreaChart(categoriesAreaChart, seriesAreaChart, '300');
        pieChart('graf3', dataGeneros, 'Género de los RockoFriends', 'RockoFriends', '300');
        pieChart('graf5', dataEngreidos, 'Por Tipos de Engreídos', 'Engreídos', '300');
        barColumnChart('graf2', categoriesPaisNacimiento, seriesPaisNacimiento, 'Según País de nacimiento', '300','bar');
        barColumnChart('graf4', categoriesPaisResidencia, seriesPaisResidencia, 'Según País de residencia', '300','bar');
        barColumnChart('graf6', categoriesDepartamentos, seriesDepartamentos, 'Según Departamentos de residencia', '300','bar');
        barColumnChart('graf8', categoriesProfesiones, seriesProfesiones, 'Según Profesiones', '1200','bar');
        barColumnChart('graf9', categoriesEdades, seriesEdades, 'Según Edades', '450','bar');
    }
    draw();
    
    var tituloDistritos = "";
    window.addEventListener('filtroPorDistritos', event => {
        var categoriesDistritos = event.detail.categoriesDistritos;
        var seriesDistritos = event.detail.seriesDistritos;
        var heightBar = 'auto';
        if (categoriesDistritos.length > 10) {
            heightBar = '700';
        }
        barColumnChart('graf7', categoriesDistritos, seriesDistritos, tituloDistritos, heightBar,'bar');
        draw();
        $('html, body').animate({
            scrollTop: $("#graf6").offset().top
        }, 1000);
        $("#modalDistritos").modal('show');
    })
</script>
