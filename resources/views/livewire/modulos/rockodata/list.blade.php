<style>
    .table thead th {
        vertical-align: middle;
    }

    .m-checkbox {
        position: relative;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /*display: block;*/
        cursor: pointer;
        padding-left: 25px;
    }

    .m-checkbox input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .m-checkbox .checkmark {
        position: absolute;
        top: 2PX;
        left: 0;
        height: 16px;
        width: 16px;
        background-color: #EEEEEE;
    }

    .m-checkbox input:checked~.checkmark {
        background-color: #F4C1E1;
    }

    .m-checkbox .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .m-checkbox input:checked~.checkmark:after {
        display: block;
        box-sizing: content-box !important;
    }

    /* Style the checkmark/indicator */
    .m-checkbox .checkmark:after {
        left: 5px;
        top: 1px;
        width: 3px;
        height: 9px;
        border: solid #FFFFFF;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<div class="container">
    <div class="row my-3 ml-1 mr-1">
        <div class="col-md-12">
            <div class="input-group px-6 py-4">
                <div class="form-outline">
                    <input class="form-control" type="text" wire:model.debounce.300ms="search"
                        placeholder="Buscar RockoFriend">
                </div>
            </div>
            <div class="card r-0 shadow">
                <div class="table-responsive">
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>RockoCard</th>

                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Documento</th>
                                <th>Género</th>
                                <th>Correo</th>
                                <th>Celular</th>

                                <th>Profesión u Oficio</th>
                                <th>Otra Profesión u oficio</th>
                                <th>Centro laboral</th>
                                <th>Estado Civil</th>
                                <th>Edad</th>
                                <th>Fecha de nacimiento</th>
                                <th>País de nacimiento</th>
                                <th>País de residencia</th>
                                <th>Departamento</th>
                                <th>Provincia</th>
                                <th>Distrito</th>


                                <th>Mascotas</th>
                                <th>RockoCoins</th>
                                <th>Fecha de registro</th>
                                <th>Hora de registro</th>
                                <th style="white-space: nowrap;">T. Datos</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($rockofriends as $rockofriend)
                                <tr>
                                    <td>{{ $rockofriend->rockocard }}</td>
                                    <td>{{ $rockofriend->nombre }}</td>
                                    <td>{{ $rockofriend->apellido }}</td>
                                    <td>{{ $rockofriend->documento }}</td>
                                    <td>{{ $rockofriend->genero }}</td>
                                    <td>{{ $rockofriend->correo }}</td>
                                    <td>{{ $rockofriend->celular }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->name }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->profesion }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->centro_laboral }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->estado_civil }}</td>
                                    <td class="text-center">{{ $rockofriend->edad }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->cumpleanos }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->pais_nacimiento }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->pais_residencia }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->id_departamento }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->id_provincia }}</td>
                                    <td>{{ $rockofriend->adicionalesdata->id_distrito }}</td>

                                    <td class="text-center">
                                        <button class="btn" type="button"
                                            wire:click='openModalMascotas({{ $rockofriend->id }},{{ count($rockofriend->mascotasdata) }})'>
                                            {{ count($rockofriend->mascotasdata) }}
                                        </button>
                                    </td>
                                    <td class="text-center">{{ $rockofriend->rockocoins }}</td>
                                    <td>{{ $rockofriend->fec_registro }}</td>
                                    <td>{{ $rockofriend->hora_registro }}</td>
                                    <td class="text-center">
                                        <label for="tyc" class="m-checkbox">
                                            <input type="checkbox" name="role[]"
                                                {{ $rockofriend->check_datos ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="21" class="py-3 italic">No hay información</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-end">
                    {{ $rockofriends->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade bd-example-modal-lg" id="modalMascotas" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mascotas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="table-responsive">
                        <table class="table table-striped table-hover r-0">
                            <thead>
                                <tr class="no-b">
                                    <th>Tipo</th>
                                    <th>Nombre</th>
                                    <th>Sexo</th>
                                    <th>Raza</th>
                                    <th>Cumpleaños</th>
                                    <th>Hobbie</th>
                                    <th>Juguete</th>
                                    <th>Comida</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($mismascotas as $mascota)
                                    <tr>
                                        <td>{{ $mascota->tipo }}</td>
                                        <td>{{ $mascota->nombre }}</td>
                                        <td>{{ $mascota->sexo }}</td>
                                        <td>{{ $mascota->raza }}</td>
                                        <td>{{ $mascota->cumpleanos }}</td>
                                        <td>{{ $mascota->hobbie }}</td>
                                        <td>{{ $mascota->juguete }}</td>
                                        <td>{{ $mascota->comida }}</td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="8" class="py-3 italic">No hay información</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.addEventListener('openModalMascotas', event => {
        $("#modalMascotas").modal('show');
    })
</script>
