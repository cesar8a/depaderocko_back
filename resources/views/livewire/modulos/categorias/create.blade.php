<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Crear Categoría</h5>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Categoría</label>
                                    <input id="name" placeholder="Categoría" class="form-control r-0 light s-12 "
                                        wire:model='categoria' type="text">
                                </div>

                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="button" wire:click='save()' class="btn w-100" style="background-color: #F4C1E1;color:#fff"><i
                                class="icon-save mr-2"></i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
