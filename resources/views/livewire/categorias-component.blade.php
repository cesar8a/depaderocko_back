<div>
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        CATEGORÍAS
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" wire:click="categoria()"><i class="icon icon-home2"></i>Categorías</span>
                    </li>

                    <li>
                        <span class="nav-link" wire:click='create()'><i class="icon icon-user-plus"></i>Nueva
                            Categoría</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.categorias.$view")
</div>
