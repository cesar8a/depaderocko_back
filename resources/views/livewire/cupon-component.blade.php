<div>
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        Cupones
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" wire:click="cupon()"><i class="icon icon-home2"></i>Cupones</span>
                    </li>
                    <li>
                        <span class="nav-link" wire:click='create()'><i class="icon icon-user-plus"></i>Crear Cupón</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.cupones.$view")


</div>