<div>
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        RockoBlog
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" wire:click="user()"><i class="icon icon-home2"></i>RockoBlogs</span>
                    </li>

                    <li>
                        <a href="{{route('createPost')}}" class="nav-link"><i class="icon icon-user-plus"></i>Nuevo
                            RockoBlog</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.posts.$view")


</div>
