<aside class="main-sidebar fixed offcanvas shadow" data-toggle='offcanvas'>
    <section class="sidebar">
        <div class="w-80px mt-3 mb-3 ml-3 " style="width:110px">
            <img src="{{ asset('assets/img/logo-letra-rosa.png') }}?v=<?php echo time(); ?>" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
                aria-controls="userSettingsCollapse"
                class="btn-fab btn-fab-sm absolute fab-right-bottom fab-top btn-primary shadow1 "
                style="background-color: #F4C1E1">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="{{ asset('assets/img/rockocorp.svg') }}" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">{{ Auth::user()->name }}</h6>
                        <a href="#"><i class="icon-circle text-primary blink"></i> En Línea</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-umbrella text-blue"></i>Cambiar Clave</a>
                        <a href="{{ route('logout') }}" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-security text-purple"></i>Cerrar Sesión</a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong>MENÚ</strong></li>
            <li><a href="{{ route('categorias') }}"><i
                        class="icon icon-account_box s-18" style="color:#edbed9"></i>Categorías</a>
            <li><a href="{{ route('posts') }}"><i class="icon icon-users s-18" style="color:#edbed9"></i>RockoBlog</a>
            <li><a href="{{ route('rockodata') }}"><i class="icon icon-address-card s-18" style="color:#edbed9"style="color:#edbed9"></i>RockoData</a>
            <li><a href="{{ route('clientes') }}"><i class="icon icon-money-1 s-18" style="color:#edbed9"></i>RockoCoins</a>
            <li><a href="{{ route('cupones') }}"><i class="icon icon-ticket s-18" style="color:#edbed9"></i>Cupones</a>
            <li><a href="{{ route('kpi') }}"><i class="icon icon-bar-chart2 s-18" style="color:#edbed9"></i>KPIs</a>
            </li>
        </ul>
    </section>
</aside>
