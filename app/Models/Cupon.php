<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
    
    protected $table = 'cupones';

    use HasFactory;
    
    protected $fillable = [
        'titulo',
        'codigo',
        'rockocoins',
        'descripcion',
        'fec_inicio',
        'fec_fin',
    ];
}
