<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoAmbiente extends Model
{
    protected $table = 'tipo_ambientes';
    use HasFactory;
    
    protected $hidden = ['created_at', 'updated_at'];

    public function ambientes_data()
    {
        return $this->belongsToMany(TipoAmbiente::class, 'ambientes')->withPivot('id','completo');
    }
}
