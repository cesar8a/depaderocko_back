<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'uuid',
        'correo',
        'clave',
        'nombre',
        'apellido',
        'documento',
        'genero',
        'celular',
        'check_terminos',
        'check_datos',
        'rockocard',
        'rockocoins'
    ];
    protected $hidden = [
        'clave',
        'remember_token',
    ];

    public function mascotasdata()
    {
        return $this->hasMany(Macosta::class, 'id_cliente', 'id');
    }

    public function adicionalesdata()
    {
        return $this->hasOne(AdicionalesCliente::class, 'id_cliente', 'id')
            ->leftjoin('profesiones', 'adicionales_clientes.id_profesion', '=', 'profesiones.id');
    }

    public function likes()
    {
        return $this->belongsToMany(Post::class, 'likes')->withTimestamps();
    }

    public static function search($search)
    {
        return empty($search)
            ? static::query()
            : static::query()
            ->whereRaw("CONCAT_WS(' ',`nombre`, `apellido`) like ?", '%' . $search . '%')
            ->orWhere('rockocard', $search);
    }
}
