<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriaPost extends Model
{
    use HasFactory;

    protected $fillable = [
        'categoria'
    ];

    public function posts(Type $var = null)
    {
        return $this->hasMany(Post::class, 'idCategoria', 'id');
    }
}
