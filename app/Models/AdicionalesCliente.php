<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdicionalesCliente extends Model
{
    use HasFactory;

    protected $fillable = [
            'id_cliente',
            'profesion',
            'estado_civil',
            'cumpleanos',
            'pais_nacimiento',
            'pais_residencia',
            'id_departamento',
            'id_provincia',
            'id_distrito',
            'domicilio',
            'profile_img',
            'profile_option',
            'id_profesion',
            'centro_laboral'
    ];
}
