<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Macosta extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_cliente',
        'tipo',
        'nombre',
        'cumpleanos',
        'sexo',
        'raza',
        'hobbie',
        'foto',
        'apodo',
        'juguete',
        'comida'
    ];
}
