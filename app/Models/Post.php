<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha',
        'imagen',
        'titulo',
        'slug',
        'contenido',
        'estado',
        'idUsuario',
        'idCategoria',
        'resumen'
    ];

    public function categoria()
    {
        return $this->hasOne(CategoriaPost::class, 'id', 'idCategoria');
    }

    public function usuario()
    {
        return $this->hasOne(User::class, 'id', 'idUsuario');
    }

    public function getImagenAttribute( $value )
    {
        return config('global.imagen_url').$value;
    }

    public function likes(){
        return $this->belongsToMany(Cliente::class, 'likes')->withTimestamps(); 
    }

    public function getFechaAttribute($value)
    {
        $date = explode('-', $value);

        switch ($date[1]) {
            case 01:
                $mes = "Enero";
                break;
            case 02:
                $mes = "Febrero";
                break;
            case 03:
                $mes = "Marzo";
                break;
            case 04:
                $mes = "Abril";
                break;
            case 05:
                $mes = "Mayo";
                break;
            case 06:
                $mes = "Junio";
                break;
            case 07:
                $mes = "Julio";
                break;
            case '08':
                $mes = "Agosto";
                break;
            case '09':
                $mes = "Septiembre";
                break;
            case 10:
                $mes = "Octubre";
                break;
            case 11:
                $mes = "Noviembre";
                break;
            case 12:
                $mes = "Diciembre";
                break;
        }

        return $date[2]. " de ".$mes. " ".$date[0];
    }
}
