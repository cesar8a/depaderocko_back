<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propiedad extends Model
{
    protected $table = 'propiedades';
    use HasFactory;

    protected $fillable = [
        'id_cliente',
        'nombre',
    ];

    public function contacto_data()
    {
        return $this->hasMany(Contacto::class, 'id_propiedad', 'id');
    }

    public function servicios_data()
    {
        return $this->belongsToMany(TipoServicio::class, 'servicios')->withTimestamps();
    }

    public function ambientes_data()
    {
        return $this->belongsToMany(TipoAmbiente::class, 'ambientes')->withPivot('id', 'descripcion', 'imagen1_url', 'imagen2_url', 'completo');
    }

    public function videos_data()
    {
        return $this->hasMany(Video::class, 'id_propiedad', 'id');
    }
}
