<?php

namespace App\Imports;

use App\Mail\InvitacionMail;
use App\Models\Macosta;
use Exception;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use stdClass;

// use DB;

class InvitacionImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $url = config('global.front_url') . "/registro";
        $emails = [];
        $config = [];
        foreach ($collection as $row) {
            if ($row['nombre'] != null && $row['correo'] != null) {
                if (in_array($row['correo'], $emails)){
                    throw ValidationException::withMessages(['correo' => 'El correo '.$row['correo'].' se REPITE en el archivo.']);
                }
                array_push($emails, $row['correo']);
                
                $to = [
                    'email' => $row['correo'],
                    'name' => $row['nombre'],
                ];
                $validator = Validator::make(
                    $to,
                    array(
                        'email' => 'required|email',
                        'name' => 'required'
                    )
                );
                if($validator->fails()){
                    throw ValidationException::withMessages($validator->errors()->all());
                }
                array_push($config, $to);
            }
        }

        if (!empty($config)) {
            foreach ($config as $c) {
                Mail::to($c['email'])->send((new InvitacionMail($url,$c['name'])));
            }
        }
    }
}
