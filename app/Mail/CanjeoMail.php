<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CanjeoMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $mensaje;
    public $sub;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($option,$cantidad)
    {
        if($option == 1){
            $this->mensaje = '¡Oink! Acabas de canjear '.$cantidad.' RockoCoins';
            $this->sub = 'Canje de Rockocoins | El Depa de Rocko';
        } else if($option == 2){
            $this->mensaje = '¡Oink! Acabas de ganar '.$cantidad.' RockoCoins';
            $this->sub = 'Ganaste Rockocoins | El Depa de Rocko';
        }
        
        $this->afterCommit();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.canjeo')
            ->subject($this->sub);
    }
}
