<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ValidarEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $url;


    public function __construct($url)
    {
        $this->url = $url;
    }


    public function build()
    {
        return $this->view('emails.registro')
            ->subject('Verifica tu correo | El Depa de Rocko');
    }
}
