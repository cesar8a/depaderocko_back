<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitacionMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $url,$nombre;
    public $emails;

    public function __construct($url,$nombre)
    {
        $this->url = $url;
        $this->nombre = $nombre;
        
        $this->afterCommit();
    }


    public function build()
    {
        return $this->view('emails.invitacion')
            ->subject('Te invitamos a registrarte | El Depa de Rocko');
    }
}
