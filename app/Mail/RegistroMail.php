<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistroMail extends Mailable implements ShouldQueue

{
    use Queueable, SerializesModels;

    public $url;

    public function __construct($url)
    {
        $this->url = $url;
        $this->afterCommit();
    }


    public function build()
    {
        return $this->view('emails.registro')
            ->subject('Verifica tu cuenta | El Depa de Rocko');
    }
}
