<?php

namespace App\Http\Livewire;

use App\Models\AdicionalesCliente;
use App\Models\Cliente;
use App\Models\Macosta;
use Livewire\Component;
use Log, DB;

class KPIComponent extends Component
{
    public $view = "graficos";
    public $fec_inicio, $fec_fin, $cantidadRockofriends, $cantidadMascotas;
    public $meses = [
        ['nombre' => 'Enero', 'active' => 'active'],
        ['nombre' => 'Febrero', 'active' => ''],
        ['nombre' => 'Marzo', 'active' => ''],
        ['nombre' => 'Abril', 'active' => ''],
        ['nombre' => 'Mayo', 'active' => ''],
        ['nombre' => 'Junio', 'active' => ''],
        ['nombre' => 'Julio', 'active' => ''],
        ['nombre' => 'Agosto', 'active' => ''],
        ['nombre' => 'Septiembre', 'active' => ''],
        ['nombre' => 'Octubre', 'active' => ''],
        ['nombre' => 'Noviembre', 'active' => ''],
        ['nombre' => 'Diciembre', 'active' => '']
    ];
    public $categoriesAreaChart, $seriesAreaChart,
        $categoriesPaisNacimiento, $seriesPaisNacimiento, $categoriesPaisResidencia, $seriesPaisResidencia,
        $categoriesDepartamentos, $seriesDepartamentos, $categoriesDistritos, $seriesDistritos, $categoriesProfesiones, $seriesProfesiones,
        $dataGeneros, $dataEngreidos, $categoriesEdades, $seriesEdades  = [];

    protected $listeners = ['filtrarPorDistritos' => 'filtrarPorDistritos'];
    public $init = 0;

    public function render()
    {
        $graficos = [];
        if ($this->view == "graficos" && $this->init == 0) {
            $this->filtrarAreaChart();
            $this->filtrarPaisNacimiento();
            $this->filtrarPaisResidencia();
            $this->filtrarGeneros();
            $this->filtrarEngreidos();
            $this->filtrarPorDepartamentos();
            $this->filtrarPorProfesion();
            $this->filtrarPorEdades();

            // $this->filtrarPorDistritos();
            $this->init = 1;
        }
        return view('livewire.k-p-i-component');
    }

    public function graficos()
    {
        // $this->filtrarAreaChart();
        // $this->view = "graficos";
    }

    public function filtrarAreaChart()
    {
        $month = date('m');
        $year = date('Y');

        $query = Cliente::where('check_terminos', true)->select(DB::raw('DATE(created_at) as name'), DB::raw('count(*) as data'));
        $query
            // ->whereIn(DB::raw('MONTH(created_at)'), [$month])
            ->whereYear('created_at', $year);

        // if ($request->fecha != null) {
        //     $query->whereBetween('fecha', [$inicio, $final]);
        // }
        // if ($request->idCategoria != null) {
        //     $query->where('idCategoria', $request->idCategoria);
        // }
        // if ($request->seacrh != null) {
        //     $query->where('titulo', 'like', "%$request->seacrh%");
        // }

        // $query->with(['categoria', 'usuario']);

        $query->groupBy('name');
        $graficos = $query->get();
        $graficos = $this->completarDias($graficos);
        $this->categoriesAreaChart = $graficos->pluck('name');
        $this->seriesAreaChart = ['name' => 'Registros', 'data' => $graficos->pluck('data')];

        $this->cantidadRockofriends = $graficos->pluck('data')->sum();
    }

    public function filtrarPaisNacimiento()
    {
        $year = date('Y');
        $graficos = Cliente::select(DB::raw('count(*) as count'), DB::raw('YEAR(clientes.created_at) year'), 'pais.name')
            ->join('adicionales_clientes', 'clientes.id', '=', 'adicionales_clientes.id_cliente')
            ->join('pais', 'pais.id', '=', 'adicionales_clientes.pais_nacimiento')
            ->where('adicionales_clientes.pais_nacimiento', '!=', null)
            ->groupBy('year', 'pais.name')
            ->orderBy('count', 'asc')
            ->orderBy('pais.name', 'asc')
            ->get();

        $this->categoriesPaisNacimiento = $graficos->pluck('name');
        $this->seriesPaisNacimiento = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesPaisNacimiento as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    $this->seriesPaisNacimiento[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']]];
                array_push($this->seriesPaisNacimiento, $newItem);
            }
        }
    }

    public function filtrarPaisResidencia()
    {
        $year = date('Y');
        $graficos = Cliente::select(DB::raw('count(*) as count'), DB::raw('YEAR(clientes.created_at) year'), 'pais.name')
            ->join('adicionales_clientes', 'clientes.id', '=', 'adicionales_clientes.id_cliente')
            ->join('pais', 'pais.id', '=', 'adicionales_clientes.pais_residencia')
            ->where('adicionales_clientes.pais_residencia', '!=', null)
            ->groupBy('year', 'pais.name')
            ->orderBy('count', 'asc')
            ->orderBy('pais.name', 'asc')
            ->get();

        $this->categoriesPaisResidencia = $graficos->pluck('name');
        $this->seriesPaisResidencia = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesPaisResidencia as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    $this->seriesPaisResidencia[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']]];
                array_push($this->seriesPaisResidencia, $newItem);
            }
        }
    }

    public function completarDias($graficos)
    {
        for ($key = 0; $key < $graficos->count(); $key++) {
            $item = $graficos[$key];

            if ($graficos->count() != $key + 1) {
                $datetime1 = date_create($item->name);
                $datetime2 = date_create($graficos[$key + 1]->name);
                $interval = date_diff($datetime1, $datetime2);
                if ($interval->format('%a') != 1) {
                    $newItem = ['name' => date('Y-m-d', strtotime(' +1 day', strtotime($item->name))), 'data' => 0];
                    $graficos->splice($key + 1, 0, [(object)$newItem]);
                }
            }
        }
        return $graficos;
    }

    public function cambiarMes()
    {
        $this->filtrar();
    }

    public function filtrarGeneros()
    {
        $graficos = Cliente::select(DB::raw('count(*) as y'), 'genero as name')
            ->groupBy('name')
            ->orderBy('y', 'desc')
            ->get();
        $graficos->map(function ($item, $key) use ($graficos) {
            switch ($item->name) {
                case 1:
                    $item->name = 'Hombre';
                    break;
                case 2:
                    $item->name = 'Mujer';
                    break;
                case 3:
                    $item->name = 'Prefiero no decirlo';
                    break;
            }
            return $item;
        });

        $this->dataGeneros = $graficos;
    }

    public function filtrarEngreidos()
    {
        $graficos = Macosta::select(DB::raw('count(*) as y'), 'tipo as name')
            ->groupBy('name')
            ->orderBy('y', 'desc')
            ->get();

        $graficos->map(function ($item, $key) use ($graficos) {
            switch ($item->name) {
                case 1:
                    $item->name = 'Gato';
                    break;
                case 2:
                    $item->name = 'Perro';
                    break;
                case 3:
                    $item->name = 'Hámster';
                    break;
                case 3:
                    $item->name = 'Conejo';
                    break;
                case 3:
                    $item->name = 'Ave';
                    break;
                case 3:
                    $item->name = 'Pez';
                    break;
                case 3:
                    $item->name = 'Puercoespín';
                    break;
                default:
                    $item->name = 'Otro';
            }
            return $item;
        });

        $this->dataEngreidos = $graficos;
        $this->cantidadMascotas = $graficos->pluck('y')->sum();
    }

    function filtrarPorDepartamentos()
    {

        // $year = date('Y');
        $graficos = AdicionalesCliente::select(DB::raw('count(*) as count'), DB::raw('YEAR(adicionales_clientes.created_at) year'), 'ubigeo_peru_departments.name', 'ubigeo_peru_departments.id')
            ->join('ubigeo_peru_departments', 'ubigeo_peru_departments.id', '=', 'adicionales_clientes.id_departamento')
            ->where('adicionales_clientes.id_departamento', '!=', null)
            ->where('adicionales_clientes.id_departamento', '!=', 0)
            ->groupBy('year', 'ubigeo_peru_departments.name', 'ubigeo_peru_departments.id')
            ->orderBy('count', 'asc')
            ->orderBy('ubigeo_peru_departments.name', 'asc')
            ->get();

        $this->categoriesDepartamentos = $graficos->pluck('name');
        $this->seriesDepartamentos = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesDepartamentos as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    array_push($item['column_key'], $graf['id']);
                    $this->seriesDepartamentos[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']], 'column_key' => [$graf['id']]];
                array_push($this->seriesDepartamentos, $newItem);
            }
        }
        // Log::info($this->seriesDepartamentos);
    }

    function filtrarPorDistritos($data)
    {
        $id_departamento = $data['id_departamento'];
        // $year = date('Y');
        $graficos = AdicionalesCliente::select(DB::raw('count(*) as count'), DB::raw('YEAR(adicionales_clientes.created_at) year'), DB::raw('ubigeo_peru_districts.name as name'))
            ->join('ubigeo_peru_districts', 'ubigeo_peru_districts.id', '=', 'adicionales_clientes.id_distrito')
            ->join('ubigeo_peru_departments', 'ubigeo_peru_districts.department_id', '=', 'ubigeo_peru_departments.id')
            ->where('adicionales_clientes.id_distrito', '!=', null)
            ->where('adicionales_clientes.id_distrito', '!=', 0)
            ->where('adicionales_clientes.id_departamento', '!=', null)
            ->where('adicionales_clientes.id_departamento', '!=', 0)
            ->where('adicionales_clientes.id_departamento', '=', $id_departamento)

            ->groupBy('year', 'ubigeo_peru_districts.name', 'ubigeo_peru_departments.name')
            ->orderBy('count', 'asc')
            ->orderBy('ubigeo_peru_districts.name', 'asc')
            ->get();
        $this->categoriesDistritos = $graficos->pluck('name');
        $this->seriesDistritos = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesDistritos as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    $this->seriesDistritos[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']]];
                array_push($this->seriesDistritos, $newItem);
            }
        }
        $this->dispatchBrowserEvent('filtroPorDistritos', ['categoriesDistritos' => $this->categoriesDistritos, 'seriesDistritos' => $this->seriesDistritos]);
    }

    function filtrarPorProfesion()
    {
        $graficos = AdicionalesCliente::select(DB::raw('count(*) as count'), DB::raw('YEAR(adicionales_clientes.created_at) year'), DB::raw('profesiones.name as name'))
            ->join('profesiones', 'profesiones.id', '=', 'adicionales_clientes.id_profesion')
            ->where('adicionales_clientes.id_profesion', '!=', null)
            ->where('adicionales_clientes.id_profesion', '!=', 0)
            ->where('adicionales_clientes.id_profesion', '!=', 1)

            ->groupBy('year', 'profesiones.name')
            ->orderBy('count', 'asc')
            ->orderBy('profesiones.name', 'asc')
            ->get();
        $this->categoriesProfesiones = $graficos->pluck('name');
        $this->seriesProfesiones = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesProfesiones as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    $this->seriesProfesiones[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']]];
                array_push($this->seriesProfesiones, $newItem);
            }
        }
    }

    public function filtrarPorEdades()
    {
        $graficos = AdicionalesCliente::select(
            DB::raw("concat(3*floor(TIMESTAMPDIFF(YEAR, cumpleanos, CURDATE())/3), ' - ', 3*floor(TIMESTAMPDIFF(YEAR, cumpleanos, CURDATE())/3) + 2) as name"),
            DB::raw('count(*) as count'),
            DB::raw('YEAR(adicionales_clientes.created_at) year')
        )
            ->groupBy('name','year')
            ->orderBy(DB::raw('TIMESTAMPDIFF(YEAR, cumpleanos, CURDATE())'), 'desc')
            ->get();
        $this->categoriesEdades = $graficos->pluck('name');
        $this->seriesEdades = [];
        foreach ($graficos as $graf) {
            $existe = false;
            foreach ($this->seriesEdades as $key => $item) {
                if ($item['name'] == $graf['year']) {
                    array_push($item['data'], $graf['count']);
                    $this->seriesEdades[$key] = $item;
                    $existe = true;
                }
            }

            if ($existe == false) {
                $newItem = ['name' => $graf['year'], 'data' => [$graf['count']]];
                array_push($this->seriesEdades, $newItem);
            }
        }
    }

    /*
    public function filtrar()
    {
        $graficos = [];
        // \DB::enableQueryLog(); // Enable query log
        if ($this->fec_inicio == null && $this->fec_fin == null) {
            // $graficos = Cliente::select(DB::raw('UNIX_TIMESTAMP(DATE(created_at))* 1000 as name'), DB::raw('count(*) as data'))
            //     ->groupBy('name')->get()->map(function ($item) {
            //         return array_values($item->toArray());
            //     });
            $graficos = Cliente::select(DB::raw('DATE(created_at) as name'), DB::raw('count(*) as data'))
                // $graficos = Cliente::select(DB::raw(`DATE_FORMAT(DATE(created_at), '%d-%m-%Y') as name`), DB::raw('count(*) as data'))
                ->groupBy('name')->get();
        } else if ($this->fec_inicio == null) {
            $graficos = Cliente::select(DB::raw('DATE(created_at) as name'), DB::raw('count(*) as data'))
                ->where('created_at', '<=', date('Y-m-d', strtotime($this->fec_fin)) . " 23:59:59")
                ->groupBy('name')->get();
        } else if ($this->fec_fin == null) {
            $graficos = Cliente::select(DB::raw('DATE(created_at) as name'), DB::raw('count(*) as data'))
                ->where('created_at', '>=', date('Y-m-d', strtotime($this->fec_inicio)) . " 00:00:00")
                ->groupBy('name')->get();
        } else {
            $graficos = Cliente::select(DB::raw('DATE(created_at) as name'), DB::raw('count(*) as data'))
                ->whereBetween('created_at', [date('Y-m-d', strtotime($this->fec_inicio)) . " 00:00:00", date('Y-m-d', strtotime($this->fec_fin)) . " 23:59:59"])
                ->groupBy('name')->get();
        }
        foreach($graficos as $key=>$item) {
        }
        // $graficos->map(function($item, $key) use ($graficos,$count) {
        //     Log::info($key.' '.$item);
        //     return $item;
        // });
            


        $categories = $graficos->pluck('name');
        $series = ['name' => 'Registros', 'data' => $graficos->pluck('data')];
        // Log::info(\DB::getQueryLog());

        // $this->dispatchBrowserEvent('filtrarGraficos', ['graficos' => $graficos]);
        $this->dispatchBrowserEvent('filtrarGraficos', ['categories' => $categories, 'series' => $series]);
    }
    */
}
