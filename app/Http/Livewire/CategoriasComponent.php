<?php

namespace App\Http\Livewire;

use App\Models\CategoriaPost;
use App\Models\Post;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

use Jantinnerezo\LivewireAlert\LivewireAlert;

class CategoriasComponent extends Component
{

    use LivewireAlert;

    public $view = "list";
    public $categoria, $idEditando;

    public function render()
    {
        
        $categorias = [];
        if($this->view == 'list'){
            $categorias = CategoriaPost::withCount('posts')->get();
        }

        return view('livewire.categorias-component', compact('categorias'));
    }

    public function categoria()
    {
        $this->view = "list";
    }

    public function create()
    {
        $this->view = "create";
    }

    public function edit($idCategoria)
    {
        $categoria = CategoriaPost::find($idCategoria);
        $this->categoria = $categoria->categoria;
        $this->idEditando = $categoria->id;
        $this->view = "edit";
    }

    public function update()
    {
        $categoria = CategoriaPost::find($this->idEditando);
        $categoria->categoria = $this->categoria;
        $categoria->save();

        $this->view = "list";
        $this->alert('success', 'Categoria editada con exito!');
    }

    public function save()
    {
        $create = CategoriaPost::create([
            'categoria' => $this->categoria
        ]);

        $this->categoria = "";
        $this->view = "list";
        $this->alert('success', 'Categoria creada con exito!');
    }

    public function eliminar($idCategoria)
    {
        $categoria = CategoriaPost::where('id',$idCategoria)->withCount('posts')->first();
        if ($categoria->posts_count > 0) {
            $this->alert('error', 'No puedes eliminar esta categoría.');
            return;
        }

        $categoria->delete();
        $this->alert('success', 'Categoria eliminada con exito!');
    }

    public function procesarEliminare($cat)
    {
        $cat->delete();

    }
}
