<?php

namespace App\Http\Livewire;

use App\Models\Cliente;
use App\Models\Macosta;
use App\Models\Pais;
use App\Models\UbigeoPeruDistricts;
use App\Models\UbigeoPeruProvinces;
use App\Models\UbigeoPeruDepartments;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Log;
use Exception;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use DB;

class RockoDataComponent extends Component
{

    public $view = "list";
    public $search = '';
    public $file, $iteration;
    public $clientid, $cantidad, $descripcion;
    public $mismascotas = [];
    public $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    use WithPagination;
    use LivewireAlert;

    public function render()
    {
        $rockofriends = [];
        if ($this->view == 'list') {
            $rockofriends = Cliente::search($this->search)->with(['mascotasdata', 'adicionalesdata'])
                ->orderBy('rockocard')->paginate(10);
            $meses = $this->meses;
            $rockofriends->map(function ($data) use ($meses) {
                if ($data->genero == 1) {
                    $data->genero = 'Hombre';
                } else if ($data->genero == 2) {
                    $data->genero = 'Mujer';
                } else if ($data->genero == 3) {
                    $data->genero = 'Prefiero no decirlo';
                }
                $dia_actual = date("Y-m-d");
                $data->edad = date_diff(date_create($data->adicionalesdata->cumpleanos), date_create($dia_actual))->format('%y');
                $date = new \DateTime($data->created_at);
                $data->fec_registro = $date->format('d/m/Y');
                $data->hora_registro = $date->format('H:i:s');

                $fecha = strtotime($data->adicionalesdata->cumpleanos);
                $data->adicionalesdata->cumpleanos = date('d', $fecha) . " de " . $meses[date('n', $fecha) - 1] . " del " . date('Y', $fecha);

                if ($data->adicionalesdata->estado_civil == 1) {
                    $data->adicionalesdata->estado_civil = 'Soltero';
                } else if ($data->adicionalesdata->estado_civil == 2) {
                    $data->adicionalesdata->estado_civil = 'Casado';
                } else if ($data->adicionalesdata->estado_civil == 3) {
                    $data->adicionalesdata->estado_civil = 'Divorciado';
                } else if ($data->adicionalesdata->estado_civil == 4) {
                    $data->adicionalesdata->estado_civil = 'Viudo';
                } else if ($data->adicionalesdata->estado_civil == 5) {
                    $data->adicionalesdata->estado_civil = 'Prefiero no decirlo';
                } else {
                    $data->adicionalesdata->estado_civil = '';
                }
                $data->adicionalesdata->pais_nacimiento = $this->matchPais($data->adicionalesdata->pais_nacimiento);
                $data->adicionalesdata->pais_residencia = $this->matchPais($data->adicionalesdata->pais_residencia);
                $data->adicionalesdata->id_departamento = $this->matchDepartamento($data->adicionalesdata->id_departamento);
                $data->adicionalesdata->id_provincia = $this->matchProvincia($data->adicionalesdata->id_provincia);
                $data->adicionalesdata->id_distrito = $this->matchDistrito($data->adicionalesdata->id_distrito);
                return $data;
            });
        }

        return view('livewire.rocko-data-component', compact('rockofriends'));
    }

    public function rockoFriends()
    {
        $this->view = 'list';
    }

    public function openModalMascotas($id = null, $count = null)
    {
        if ($id == null || $count == 0) {
            return;
        }
        $mismascotas = Macosta::where('id_cliente', $id)->get();
        if ($mismascotas == null) {
            return;
        }
        $meses = $this->meses;
        $mismascotas->map(function ($data) use ($meses) {
            if ($data->tipo == 1) {
                $data->tipo = 'Gato';
            } else if ($data->tipo == 2) {
                $data->tipo = 'Perro';
            } else if ($data->tipo == 3) {
                $data->tipo = 'Hámster';
            } else if ($data->tipo == 4) {
                $data->tipo = 'Conejo';
            } else if ($data->tipo == 5) {
                $data->tipo = 'Ave';
            } else if ($data->tipo == 6) {
                $data->tipo = 'Pez';
            } else if ($data->tipo == 7) {
                $data->tipo = 'Puercoespín';
            } else if ($data->tipo == 8) {
                $data->tipo = 'Otro';
            }
            if ($data->sexo == 1) {
                $data->sexo = 'Macho';
            } else if ($data->sexo == 2) {
                $data->sexo = 'Hembra';
            } else {
                $data->sexo = '';
            }

            if ($data->cumpleanos != null) {
                $fecha = strtotime($data->cumpleanos);
                $data->cumpleanos = date('d', $fecha) . " de " . $meses[date('n', $fecha) - 1] . " del " . date('Y', $fecha);
            }
            return $data;
        });
        $this->mismascotas = $mismascotas;
        $this->dispatchBrowserEvent('openModalMascotas');
    }

    function matchPais($pais)
    {
        if ($pais != null && $pais != 0) {
            if ($pais == '174') {
                $pais = 'Perú';
            } else {
                $pais = Pais::select('name')->find($pais)->name;
            }
        } else {
            $pais = '';
        }
        return $pais;
    }

    function matchDepartamento($departamento)
    {
        if ($departamento != null && $departamento != 0) {
            if ($departamento == '15') {
                $departamento = 'Lima';
            } else {
                $departamento = UbigeoPeruDepartments::select('name')->find($departamento)->name;
            }
        } else {
            $departamento = '';
        }
        return $departamento;
    }

    function matchProvincia($provincia)
    {
        if ($provincia != null && $provincia != 0) {
            if ($provincia == '1501') {
                $provincia = 'Lima';
            } else {
                $provincia = UbigeoPeruProvinces::select('name')->find($provincia)->name;
            }
        } else {
            $provincia = '';
        }
        return $provincia;
    }

    function matchDistrito($distrito)
    {
        if ($distrito != null && $distrito != 0) {
            if ($distrito == '150101') {
                $distrito = 'Lima';
            } else {
                $distrito = UbigeoPeruDistricts::select('name')->find($distrito)->name;
            }
        } else {
            $distrito = '';
        }
        return $distrito;
    }
}
