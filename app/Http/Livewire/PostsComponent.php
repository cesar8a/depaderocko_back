<?php

namespace App\Http\Livewire;

use App\Models\CategoriaPost;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class PostsComponent extends Component
{
    use WithFileUploads;

    use LivewireAlert;
    public $view = "list";
    public $categorias = [], $idCategoria, $resumen, $descripcion, $imagen, $titulo, $idEditando, $estado;
    public function render()
    {
        $posts = [];
        if($this->view == "list"){
            $posts = Post::with(['usuario', 'categoria'])->get();
        }
        return view('livewire.posts-component', compact('posts'));
    }

    public function user()
    {
        $this->view = "list";
    }


    public function create()
    {
        $this->categorias = CategoriaPost::all();
        $this->emitUp('postAdded');
        $this->view = "create";
    }

    public function save()
    {
        $imagen = $this->imagen->store('photos');

        $post = Post::create([
            'fecha' => date('Y-m-d'),
            'imagen' => $imagen,
            'titulo' => $this->titulo,
            'slug' => $this->slug($this->titulo),
            'contenido' => $this->descripcion,
            'estado' => 1,
            'idUsuario' => Auth::user()->id,
            'idCategoria' => $this->idCategoria,
            'resumen' => $this->resumen
        ]);

        $this->view = "list";
        $this->alert('success', 'Post creado con exito!');
    }

    public function updatedPhoto()
    {
        $this->validate([
            'imagen' => 'image|max:1024', // 1MB Max
        ]);
    }

    public function edit($idPost)
    {
        $this->categorias = CategoriaPost::all();
        $post = Post::find($idPost);
        $this->titulo = $post->titulo;
        $this->resumen = $post->resumen;
        $this->descripcion = $post->contenido;
        $this->slug = $post->slug;
        $this->idEditando = $post->id;
        $this->idCategoria = $post->idCategoria;
        $this->fecha = $post->fecha;
        $this->estado = $post->estado;
        $this->view = "edit";
    }

    public function update()
    {
        $post = Post::find($this->idEditando);
        $post->titulo = $this->titulo;
        $post->fecha = $this->fecha;
        $post->resumen = $this->resumen;
        $post->contenido = $this->descripcion;
        $post->slug = $this->slug;
        $post->idCategoria = $this->idCategoria;
        $post->estado = $this->estado;
        if($this->imagen != null) {
            $post->imagen = $this->imagen->store('photos');
        }
        $post->save();

        $this->view = "list";
        $this->alert('success', 'Post editado con exito!');
    }

    public function slug($texto)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $texto);
        $slug = strtolower($slug);
        return $slug;
    }

    public function eliminar($idPost)
    {
        $post = Post::find($idPost);
        $post->delete();

        $this->alert('success', 'Post eliminado con exito!');
    }
}
