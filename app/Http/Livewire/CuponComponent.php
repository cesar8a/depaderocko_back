<?php

namespace App\Http\Livewire;

use App\Models\Cupon;
use App\Models\HistorialCoin;
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use DB;
use Illuminate\Support\Facades\Log;

class CuponComponent extends Component
{
    public $view = "list";
    public $titulo, $codigo, $descripcion, $fec_inicio, $fec_fin, $rockocoins;
    public $idEditando;
    public $idEliminar;
    use LivewireAlert;

    public function render()
    {
        $cupones = [];
        if($this->view == 'list'){
            $cupones = Cupon::latest()->get();
        }
        return view('livewire.cupon-component', compact('cupones'));
    }

    public function cupon()
    {
        $this->view = "list";
    }

    public function edit($idcupon)
    {
        $this->idEditando = $idcupon;
        $cupon = Cupon::find($this->idEditando);
        $this->titulo = $cupon->titulo;
        $this->codigo = $cupon->codigo;
        $this->rockocoins = $cupon->rockocoins;
        $this->descripcion = $cupon->descripcion;
        $this->fec_inicio = $cupon->fec_inicio;
        $this->fec_fin = $cupon->fec_fin;
        $this->view = "edit";
    }

    public function updateCupon()
    {
        try {
            DB::beginTransaction();
            $this->validate([
                'titulo' => 'required',
                'rockocoins' => 'required|integer|max:500|min:1',
                'descripcion' => 'required',
                'fec_inicio' => 'required|before:fec_fin',
                'fec_fin' => 'required|after:fec_inicio',
            ]);
            $cupon = Cupon::find($this->idEditando);
            $cupon->titulo = $this->titulo;
            $cupon->rockocoins = $this->rockocoins;
            $cupon->descripcion = $this->descripcion;
            $cupon->fec_inicio = $this->fec_inicio;
            $cupon->fec_fin = $this->fec_fin;
            $cupon->save();
            DB::commit();

            $this->view = "list";
            $this->alert('success', 'Cupón editado con exito!');
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', $e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Ha ocurrido un error inesperado.');
        }
    }

    public function create()
    {
        $this->reset(['titulo', 'codigo', 'descripcion', 'fec_inicio', 'fec_fin', 'rockocoins']);
        $this->view = "create";
    }

    public function registrarCupon()
    {
        DB::beginTransaction();
        try {
            $this->validate([
                'titulo' => 'required',
                'rockocoins' => 'required|integer|max:500|min:1',
                'codigo' => 'required|unique:cupones,codigo',
                'descripcion' => 'required',
                'fec_inicio' => 'required|before:fec_fin',
                'fec_fin' => 'required|after:fec_inicio',
            ]);
            Cupon::create([
                'titulo' => $this->titulo,
                'codigo' => $this->codigo,
                'descripcion' => $this->descripcion,
                'fec_inicio' => $this->fec_inicio,
                'fec_fin' => $this->fec_fin,
                'rockocoins' => $this->rockocoins
            ]);
            DB::commit();
            $this->view = "list";
            $this->alert('success', 'Se registró el cupón correctamente!');
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', $e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Ha ocurrido un error inesperado.');
        }
    }

    public function eliminar($idcupon)
    {
        if ($idcupon == null) {
            return;
        }
        $this->idEliminar = $idcupon;
        $this->dispatchBrowserEvent('openModalEliminarCupon');
    }

    public function eliminarCupon()
    {
        DB::beginTransaction();
        try {
            if ($this->idEliminar == null) {
                return;
            }

            $cupon = Cupon::find($this->idEliminar);
            $historyCount = HistorialCoin::where('id_cupon',$this->idEliminar)->count();
            if($historyCount != 0){
                $this->alert('error', 'No puedes eliminar este cupón porque ya ha sido reclamado.');
                return;
            }
            $cupon->delete();
            DB::commit();
            $this->view = "list";
            $this->dispatchBrowserEvent('closeModalEliminarCupon');
            $this->alert('success', 'Se eliminó el cupón correctamente!');
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', $e->getMessage());
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('[CuponComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Ha ocurrido un error inesperado.');
        }
    }
}
