<?php

namespace App\Http\Livewire;

use App\Models\Cliente;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Log;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\InvitacionImport;
use App\Mail\CanjeoMail;
use App\Models\HistorialCoin;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use DB;
use Illuminate\Support\Facades\Mail;

class ClienteComponent extends Component
{
    public $view = "list";
    public $search = '';
    public $file, $iteration;
    public $clientid, $cantidad, $descripcion;
    use WithPagination;
    use WithFileUploads;
    use LivewireAlert;

    public function render()
    {
        $clientes = [];
        if ($this->view == 'list') {
            $clientes = Cliente::search($this->search)->orderBy('rockocard')->paginate(10);
        }
        return view('livewire.cliente-component', compact('clientes'));
    }

    public function cliente()
    {
        $this->view = "list";
    }

    public function import()
    {
        $this->view = "import";
    }

    public function enviarInvitacion()
    {
        DB::beginTransaction();
        try {
            $this->validate([
                'file' => 'required|max:10000|mimes:xlsx,xls',
            ]);
            Excel::import(new InvitacionImport, $this->file);
            $this->alert('success', 'Se envió la invitación correctamente!');
            $this->file = null;
            $this->iteration++;
            $this->view = "import";
            DB::commit();
        } catch (\Illuminate\Validation\ValidationException $e) {
            $this->file = null;
            $this->iteration++;
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', $e->getMessage());
        } catch (\InvalidArgumentException $e) {
            $this->file = null;
            $this->iteration++;
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Formato invalido en alguna columna.');
        } catch (\Exception $e) {
            $this->file = null;
            $this->iteration++;
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Debe seleccionar un archivo con el formato correcto.');
        }
    }

    public function edit()
    {
    }

    public function modalUsarRockocoins($id = null)
    {
        if ($id == null) {
            return;
        }
        $this->clientid = $id;
        $this->cantidad = null;
        $this->descripcion = null;
        $this->dispatchBrowserEvent('openModalUsarRockocoins');
    }

    public function usarRockoCoins()
    {
        DB::beginTransaction();
        try {
            $this->validate([
                'cantidad' => 'required|integer|max:500|min:1',
                'descripcion' => 'required',
            ]);
            // validar cantidad de rockocoins
            $cliente = Cliente::select(['id', 'rockocoins', 'correo'])->where('id', $this->clientid)->first();
            if ($this->cantidad > $cliente->rockocoins) {
                throw \Illuminate\Validation\ValidationException::withMessages(['cantidad' => 'No tiene los RockoCoins suficientes.']);
            }

            // registrar historial de usos
            HistorialCoin::create([
                'id_cliente' => $this->clientid,
                'rockocoins' => $this->cantidad,
                'accion' => config('global.action_decrease_rockocoins'),
                'descripcion' => $this->descripcion
            ]);

            // descontar de rockocoins del cliente
            $cliente->rockocoins = ($cliente->rockocoins - $this->cantidad);
            $cliente->save();

            // Enviar correo
            Mail::to($cliente->correo)->send((new CanjeoMail(1,$this->cantidad))->afterCommit());

            DB::commit();
            $this->dispatchBrowserEvent('closeModalUsarRockocoins');
            $this->alert('success', 'Se descontaron las RockoCoins correctamente!');
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            // dd(array_values($e->errors())[0]);
            $this->alert('error', array_values($e->errors())[0][0]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Ha ocurrido un error inesperado.');
        }
    }

    public function modalAgregarRockocoins($id = null)
    {
        if ($id == null) {
            return;
        }
        $this->clientid = $id;
        $this->cantidad = null;
        $this->descripcion = null;
        $this->dispatchBrowserEvent('openModalAgregarRockocoins');
    }

    public function agregarRockocoins()
    {
        DB::beginTransaction();
        try {
            $this->validate([
                'cantidad' => 'required|integer|max:500|min:1',
                'descripcion' => 'required',
            ]);

            $cliente = Cliente::select(['id', 'rockocoins', 'correo'])->where('id', $this->clientid)->first();

            // registrar historial de usos
            HistorialCoin::create([
                'id_cliente' => $this->clientid,
                'rockocoins' => $this->cantidad,
                'accion' => config('global.action_add_rockocoins'),
                'descripcion' => $this->descripcion
            ]);

            $cliente->rockocoins = ($cliente->rockocoins + $this->cantidad);
            $cliente->save();

            // Enviar correo
            Mail::to($cliente->correo)->send((new CanjeoMail(2,$this->cantidad))->afterCommit());

            DB::commit();
            $this->dispatchBrowserEvent('closeModalAgregarRockocoins');
            $this->alert('success', 'Se agregaron las RockoCoins correctamente!');
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            // dd(array_values($e->errors())[0]);
            $this->alert('error', array_values($e->errors())[0][0]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('[ClienteComponent] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            $this->alert('error', 'Ha ocurrido un error inesperado.');
        }
    }
}
