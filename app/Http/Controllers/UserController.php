<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $val = User::whereEmail($request->usu_correo)->wherePassword(sha1($request->usu_pass))->count();
        if ($val > 0) {
            $user = User::whereEmail($request->usu_correo)->wherePassword(sha1($request->usu_pass))->first();

            Auth::loginUsingId($user->id);
            return redirect('home');
        } else {
            return back()->with('success', 'Los datos especificados no existen.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }

    public function getAll()
    {
        return response()->json([
            'data' => User::all()
        ]);
    }
}
