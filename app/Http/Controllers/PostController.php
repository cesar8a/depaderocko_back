<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\{CategoriaPost, Post, Cliente};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DB;

class PostController extends Controller
{
    public function getAll(Request $request)
    {
        if($request->uuid != null){
            $cliente_id = Cliente::select('id')->where('uuid', $request->uuid)->first()->id;
            $posts = Post::with(['categoria', 'usuario'])
            ->select('posts.id','posts.idCategoria','posts.idUsuario','posts.titulo','posts.imagen','posts.resumen','posts.slug','posts.views',
            DB::raw('CASE WHEN likes.cliente_id IS NULL THEN FALSE ELSE TRUE END as like_cliente'))
            ->withCount('likes')
            ->leftjoin('likes', function ($join) use($cliente_id) {
                $join->on('posts.id', '=', 'likes.post_id')
                ->where('likes.cliente_id',$cliente_id);
            })
            ->whereEstado(1)->orderBy('posts.id', 'desc')->paginate(6);
        } else {
            $posts = Post::with(['categoria', 'usuario'])
            ->select('posts.id','posts.idCategoria','posts.idUsuario','posts.titulo','posts.imagen','posts.resumen','posts.slug','posts.views',
            DB::raw('FALSE as like_cliente'))
            ->withCount('likes')
            ->whereEstado(1)->orderBy('posts.id', 'desc')->paginate(6);
        }

        return response()->json([
            'data' => $posts,
            'status' => 200
        ]);
    }

    public function getPost($slug)
    {
        try {
            DB::beginTransaction();
        
            if ($slug == null) {
                return response()->json([
                    'status' => false
                ]);
            }
            $post = Post::whereSlug($slug)->with(['usuario', 'categoria'])->withCount('likes')->first();
            if ($post == null) {
                return response()->json([
                    'status' => false
                ]);
            }

            $post->views++;
            $post->save();
    
            $relacionados = Post::where('idCategoria', $post->idCategoria)->whereNot('id', $post->id)->take(9)->get();
            $count = $relacionados->count();
            
            DB::commit();
            return response()->json([
                'status' => true,
                'data' => $post,
                'relacionados' => $relacionados,
                'relacion' => $count
            ]);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    public function index()
    {
        return view('modulos.postsModule');
    }

    public function createPost()
    {
        $categorias = CategoriaPost::all();
        return view('livewire.modulos.posts.createPost', compact('categorias'));
    }

    public function editPost($slug)
    {
        $post = Post::whereSlug($slug)->first();

        $fec = explode(' ', $post->fecha);

        switch ($fec[2]) {
            case "Enero":
                $m = "01";
                break;
            case "Febrero":
                $m = "02";
                break;
            case "Marzo":
                $m = "03";
                break;
            case "Abril":
                $m = "04";
                break;
            case "Mayo":
                $m = "05";
                break;
            case "Junio":
                $m = "06";
                break;
            case "Julio":
                $m = "07";
                break;
            case "Agosto":
                $m = "08";
                break;
            case "Septiembre":
                $m = "09";
                break;
            case "Octubre":
                $m = "10";
                break;
            case "Noviembre":
                $m = "11";
                break;
            case "Diciembre":
                $m = "12";
                break;
        }
        $fecha = $fec[3] . "-" . $m . "-" . $fec[0];

        $categorias = CategoriaPost::all();
        return view('livewire.modulos.posts.editPost', compact('categorias', 'post', 'fecha'));
    }

    public function store(Request $request)
    {
        $file = $request->file('imagen');
        $post = Post::create([
            'fecha' => date('Y-m-d'),
            'imagen' => $this->upload_global($file),
            'titulo' => $request->titulo,
            'slug' => $this->slug($request->titulo),
            'resumen' => $request->resumen,
            'contenido' => $request->descripcion,
            'estado' => 1,
            'idUsuario' => Auth::user()->id,
            'idCategoria' => $request->idCategoria,
        ]);

        return redirect()->route('posts');
    }

    public function update(Request $request)
    {
        $post = Post::find($request->idPost);
        $post->titulo = $request->titulo;
        $post->fecha = $request->fecha;
        $post->resumen = $request->resumen;
        $post->contenido = $request->descripcion;
        $post->slug = $request->slug;
        $post->idCategoria = $request->idCategoria;
        $post->estado = $request->estado;
        if ($request->imagen != null) {
            $post->imagen = $this->upload_global($request->file('imagen'));
        }
        $post->save();

        return redirect()->route('posts');
    }

    public function searchPosts(Request $request)
    {
        $inicio = $request->fecha . '-01';
        $final = $request->fecha . '-31';

        $query = Post::where('estado', 1);
        if ($request->fecha != null) {
            $query->whereBetween('fecha', [$inicio, $final]);
        }
        if ($request->idCategoria != null) {
            $query->where('idCategoria', $request->idCategoria);
        }
        if ($request->seacrh != null) {
            $query->where('titulo', 'like', "%$request->seacrh%");
        }

        $query->with(['categoria', 'usuario']);

        if($request->uuid != null){
            $cliente_id = Cliente::select('id')->where('uuid', $request->uuid)->first()->id;
            $query
            ->select('posts.id','posts.idCategoria','posts.idUsuario','posts.titulo','posts.imagen','posts.resumen','posts.slug','posts.views',
            DB::raw('CASE WHEN likes.cliente_id IS NULL THEN FALSE ELSE TRUE END as like_cliente'))
            
            ->leftjoin('likes', function ($join) use($cliente_id) {
                $join->on('posts.id', '=', 'likes.post_id')
                ->where('likes.cliente_id',$cliente_id);
            })
            ->orderBy('posts.id', 'desc');
        } else {
            $query
            ->select('posts.id','posts.idCategoria','posts.idUsuario','posts.titulo','posts.imagen','posts.resumen','posts.slug','posts.views',
            DB::raw('FALSE as like_cliente'))
            ->orderBy('posts.id', 'desc');
        }
        $result = $query->withCount('likes')->paginate(6);

        return response()->json([
            'data' => $result,
            'status' => 200
        ]);
    }


    function upload_global($file)
    {

        $file_type = $file->getClientOriginalExtension();
        $destinationPath = public_path() . '/images/posts/';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }

    public function slug($texto)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $texto);
        $slug = strtolower($slug);
        return $slug;
    }

    function likePost(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'uuid' => 'required',
                'idPost' => 'required',
            ]);

            $post = Post::select('id')->where('posts.id','=',$request->idPost)->withCount('likes')->first();
            $idCliente = Cliente::select('id')->where('uuid', $request->uuid)->first()->id;
            $count = $post->likes_count;
            $exists = $post->likes()->where('cliente_id', $idCliente)->exists();
            if (!$exists) {
                $count++;
                $post->likes()->attach($idCliente);
            }
            DB::commit();
            return response()->json(['status' => true, 'likes_count' => $count], 200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function unlikePost(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'uuid' => 'required',
                'idPost' => 'required',
            ]);

            $post = Post::select('id')->where('id','=',$request->idPost)->withCount('likes')->first();
            $idCliente = Cliente::select('id')->where('uuid', $request->uuid)->first()->id;
            $count = $post->likes_count;
            $exists = $post->likes()->where('cliente_id', $idCliente)->exists();
            if ($exists) {
                $count--;
                $post->likes()->detach($idCliente);
            }
            DB::commit();
            return response()->json(['status' => true, 'likes_count' => $count], 200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function checkLike(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'uuid' => 'required',
                'idPost' => 'required',
            ]);

            $idCliente = Cliente::select('id')->where('uuid', $request->uuid)->first()->id;
            $post = Post::select('id')->where('id','=',$request->idPost)->first();
            $exists = $post->likes()->where('cliente_id', $idCliente)->exists();
            $like = false;
            if ($exists) {
                $like = true;
            }
            DB::commit();
            return response()->json(['status' => true, 'like' => $like], 200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
