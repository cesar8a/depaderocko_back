<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Cliente;
use App\Models\Macosta;
use App\Models\HistorialCoin;
use Exception;
use ValidationException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MacostaController extends Controller
{
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $request->validate([
                // 'apodo' => 'required',
                'tipo_mascota' => 'required|not-in:0',
                'nombre' => 'required',
            ]);

            $cliente = Cliente::where('uuid', $request->uuid)->first();
            if ($cliente === null) {
                // user doesn't exist
                throw new Exception('El cliente no existe.');
            }
            if( $request->id_mascota && $request->id_mascota !== null ){
                $mascota = Macosta::where('id', $request->id_mascota)->first();
                if ($mascota === null) {
                    // mascota doesn't exist
                    throw new Exception('La mascota no existe.');
                }
                $mascota->tipo= $request->tipo_mascota;
                $mascota->nombre= $request->nombre;
                $mascota->cumpleanos= $request->cumple;
                $mascota->sexo= $request->sexo;
                $mascota->raza= $request->raza;
                $mascota->hobbie= $request->hobbie;
                $mascota->juguete= $request->juguete;
                $mascota->comida= $request->comida;
                // $mascota->apodo= $request->apodo;
                $mascota->save();
            } else {
                $mascota = Macosta::create([
                    'id_cliente' => $cliente->id,
                    'tipo' => $request->tipo_mascota,
                    'nombre' => $request->nombre,
                    'cumpleanos' => $request->cumple,
                    'sexo' => $request->sexo,
                    'raza' => $request->raza,
                    'hobbie' => $request->hobbie,
                    'juguete' => $request->juguete,
                    'comida' => $request->comida
                    // 'foto' => $request->foto,
                    // 'apodo' => $request->apodo
                ]);
                if(!$cliente->mascota){
                    HistorialCoin::create([
                        'id_cliente' => $cliente->id,
                        'rockocoins' => config('global.rockocoins_mascota'),
                        'accion' => config('global.action_add_rockocoins'),
                        'descripcion' => 'Mascota'
                    ]);
                    $cliente->rockocoins += config('global.rockocoins_mascota');
                    $cliente->mascota = true;
                    $cliente->save();
                }
            }

            DB::commit();
            return response()->json(['status' => true, 'mascota' => $mascota, 'rockocoins' => $cliente->rockocoins], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[MacostaController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        } catch (\Exception $e) {
            Log::error('[MacostaController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    public function getMascotas($uuid)
    {
        $cliente = Cliente::where('uuid', $uuid)->first();
        $mascotas = Macosta::where('id_cliente', $cliente->id)->get();
        return $mascotas;
    }

    public function eliminar(Request $request){
        try {
            DB::beginTransaction();
        
            $mascota = Macosta::where('id', $request->id_mascota)->first();
            if ($mascota === null) {
                // user doesn't exist
                return 100;
            }
            $mascota->delete();

            DB::commit();
            return 200;
        } catch (\Exception $e) {
            Log::error('[MacostaController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return 100;
            // return response()->json(['status'=>'false', 'message'=>$e->getMessage(), 'data'=>[]],500);
        }
    }
}
