<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Propiedad;
use App\Models\TipoServicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class ServicioController extends Controller
{
    function getServiciosPropiedad(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $servicios = $propiedad->servicios_data;
            return response()->json(['status' => true, 'servicios' => $servicios], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarServicios(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
                'servicios' => 'required_unless:servicios,null|array',
            ]);
            DB::beginTransaction();
            
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $propiedad->servicios_data()->sync($request->servicios);

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getTipoServicios(Request $request)
    {
        $tipo_servicios = TipoServicio::all();
        return response()->json(['status' => true, 'tipo_servicios' => $tipo_servicios], 200);
    }
}
