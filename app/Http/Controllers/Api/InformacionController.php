<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Informacion;
use App\Models\Propiedad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class InformacionController extends Controller
{
    function getInformacion(Request $request)
    {
        try {
            $request->validate([
                'id_informacion' => 'required',
            ]);
            $informacion = Informacion::where('id_propiedad', $request->id_propiedad)->first();
            if ($informacion == null) {
                throw new Exception('No se encontraron los datos');
            }
            return response()->json(['status' => true, 'informacion' => $informacion], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarInformacion(Request $request)
    {
        try {
            $request->validate([
                'id_informacion' => 'required',
                'id_tipo_propiedad' => 'required',
                'id_tipo_depa' => 'required_if:id_tipo_propiedad,1',
                'vista_calle' => 'required',
                'old_years' => 'required',
                'pisos' => 'required',
                'area_total' => 'required',
                'ascensor' => 'required',
                'directo' => 'required_if:ascensor,1',
                'id_pais' => 'required'
            ]);
            DB::beginTransaction();

            $informacion = Informacion::findOrFail($request->id_informacion);
            $informacion->id_tipo_propiedad = $request->id_tipo_propiedad;
            $informacion->id_tipo_depa = $request->id_tipo_depa;
            $informacion->vista_calle = $request->vista_calle;
            $informacion->old_years = $request->old_years;
            $informacion->pisos = $request->pisos;
            $informacion->area_total = $request->area_total;
            $informacion->ascensor = $request->ascensor;
            $informacion->directo = $request->directo;
            $informacion->id_pais = $request->id_pais;
            $informacion->id_departamento = $request->id_departamento;
            $informacion->id_provincia = $request->id_provincia;
            $informacion->id_distrito = $request->id_distrito;
            $informacion->referencia = $request->referencia;
            $informacion->save();

            if ($informacion->propiedad->completo_informacion == 0) {
                $propiedad = Propiedad::findOrFail($informacion->id_propiedad);
                if (
                    $propiedad->direccion != null && $propiedad->maps_direccion != null &&
                    $propiedad->maps_latitude != null && $propiedad->maps_longitude != null
                ) {
                    $propiedad->completo_informacion = 1;
                    $propiedad->save();
                }
            }

            DB::commit();

            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarUbicacion(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
                'direccion' => 'required',
                'maps_direccion' => 'required_if:id_tipo_propiedad,1',
                'maps_latitude' => 'required',
                'maps_longitude' => 'required'
            ]);
            DB::beginTransaction();

            $propiedad = Propiedad::findOrFail($request->id_propiedad);
            $propiedad->direccion = $request->direccion;
            $propiedad->maps_direccion = $request->maps_direccion;
            $propiedad->maps_latitude = $request->maps_latitude;
            $propiedad->maps_longitude = $request->maps_longitude;

            if ($propiedad->completo_informacion == 0) {
                $informacion = Informacion::where('id_propiedad', $propiedad->id);
                if (
                    $informacion->id_tipo_propiedad != null && $informacion->vista_calle != null &&
                    $informacion->old_years != null && $informacion->pisos != null &&
                    $informacion->area_total != null && $informacion->ascensor != null &&
                    $informacion->id_pais != null
                ) {
                    $propiedad->completo_informacion = 1;
                }
            }

            $propiedad->save();

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[InformacionController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getTipoPropiedades(Request $request)
    {
        $tipo_propiedades = DB::select('select * from tipo_propiedades');
        return response()->json(['status' => true, 'tipo_propiedades' => $tipo_propiedades], 200);
    }

    function getTipoDepas(Request $request)
    {
        $tipo_depas = DB::select('select * from tipo_depas');
        return response()->json(['status' => true, 'tipo_depas' => $tipo_depas], 200);
    }
}
