<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use App\Mail\{RegistroMail, ValidarEmail, RecuperarContrasena};
use App\Models\AdicionalesCliente;
use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\HistorialCoin;
use DateTime;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;


use Mail;

class ClienteController extends Controller
{

    public function login(Request $request)
    {
        $cliente = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->count();

        if ($cliente > 0) {
            $cliente = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->where('verificacion_correo', 2)->count();

            if ($cliente > 0) {
                $cliente = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->first();
                $token = $cliente->createToken("auth_token")->plainTextToken;
                return response()->json(["status"=>true, "msg"=>"ok", "access_token"=>$token, "uuid"=>$cliente->uuid]);
            } else {
                return 101;
            }
        } else {
            return 100;
        }
    }

    public function resetAccount($correo)
    {
        $val = Cliente::whereCorreo($correo)->count();

        if ($val > 0) {
            $val = Cliente::whereCorreo($correo)->first();
            $val->verificacion_correo = 1;
            $val->clave = "";
            $val->save();


            $url = config('global.front_url') . "/nueva-clave/" . $val->uuid;
            if (Mail::to($correo)->send(new RecuperarContrasena($url))) {
                return 200;
            }
        } else {
            return 100;
        }
    }

    function asignarClave(Request $request)
    {
        $usuario = Cliente::where('uuid', $request->uuid)->first();
        $usuario->clave = sha1($request->clave);
        $usuario->verificacion_correo = 2;
        $usuario->save();

        return 200;
    }

    function getAge($fecha)
    {

        $mayor = 18;

        //Creamos objeto fecha desde los valores recibidos
        $nacio = DateTime::createFromFormat('Y-m-d', $fecha);
        $actual = new DateTime();
        if ($actual < $nacio) {
            return false;
        } else {
            //Calculamos usando diff y la fecha actual
            $calculo = $nacio->diff(new DateTime());

            //Obtenemos la edad
            $edad =  $calculo->y;

            if ($edad < $mayor) {
                return false;
                // return "Usted es menor de edad. Su edad es: $edad\n";
            } else {
                return true;
                // return "Usted es mayor de edad. Su edad es: $edad\n";
            }
        }
    }

    public function registrer(Request $request)
    {
        try {
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                // 'tipo_documento' => 'required',
                'documento' => 'required|unique:clientes',
                // 'documento' => [
                //     'required',
                //     Rule::unique('clientes')->where(function ($query) use ($request) {
                //         return $query->where('tipo_documento', $request->tipo_documento)
                //             ->where('documento', $request->documento);
                //     })
                // ],
                'genero' => 'required',
                'celular' => 'required',
                'clave' => 'required',
                'correo' => 'required|unique:clientes',
                'check_terminos' => 'required',
                'check_datos' => 'required',
                'fecha' => 'required'
            ]);

            // if ($request->tipo_documento == 1) {
            //     if (strlen($request->documento) != 8) {
            //         throw new Exception("Número de documento inválido.");
            //     }
            // } else if ($request->tipo_documento == 2) {
            //     if (strlen($request->documento) != 12) {
            //         throw new Exception("Número de documento inválido.");
            //     }
            // }
            if (!$this->getAge($request->fecha) || ceil(log10($request->celular)) > 15 || ceil(log10($request->documento)) > 20 || !$request->check_terminos) {
                return 100;
            }
            DB::beginTransaction();
            $rockocard = Cliente::max('rockocard') + 1;
            $registro = Cliente::create([
                'uuid' => Uuid::generate()->string,
                'correo' => $request->correo,
                'clave' => sha1($request->clave),
                'nombre' => ucwords($request->nombre),
                'apellido' => ucwords($request->apellido),
                // 'tipo_documento' => $request->tipo_documento,
                'documento' => $request->documento,
                'genero' => $request->genero,
                'celular' => $request->celular,
                'check_terminos' => $request->check_terminos,
                'check_datos' => $request->check_datos,
                'rockocard' =>  $rockocard
            ]);

            AdicionalesCliente::create([
                'id_cliente' => $registro->id,
                'cumpleanos' => $request->fecha,
            ]);


            HistorialCoin::create([
                'id_cliente' => $registro->id,
                'rockocoins' => config('global.rockocoins_registro'),
                'accion' => config('global.action_add_rockocoins'),
                'descripcion' => 'Registro'
            ]);


            $url = config('global.front_url') . "/confirmar-correo/" . $registro->uuid;
            Mail::to($request->correo)->send((new RegistroMail($url))->afterCommit());

            DB::commit();
            return 200;
            // return response()->json(['status'=>'true', 'message'=>$e->getMessage(), 'data'=>[]],200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return 100;
            // return response()->json(['status'=>'false', 'message'=>$e->getMessage(), 'data'=>[]],500);
        }
    }

    public function storeAdicionales(Request $request)
    {
        try {
            $request->validate([
                'uuid' => 'required',
                'cumpleanos' => 'required',
                'nacimiento' => 'required',
                'pais' => 'required',
            ]);

            DB::beginTransaction();

            $cliente = Cliente::where('uuid', $request->uuid)->first();
            if ($cliente === null) {
                // user doesn't exist
                return 100;
            }

            $adicionales = AdicionalesCliente::where('id_cliente', $cliente->id)->first();

            if ($adicionales === null) {
                return 100;
            }

            $adicionales->id_profesion = $request->id_profesion;
            $adicionales->profesion = $request->profesion != null ? ucwords($request->profesion) : null;
            $adicionales->centro_laboral = $request->centro_laboral != null ? ucwords($request->centro_laboral) : null;

            $adicionales->estado_civil = $request->estado_civil;
            $adicionales->cumpleanos = $request->cumpleanos;
            $adicionales->pais_nacimiento = $request->nacimiento;
            $adicionales->pais_residencia = $request->pais;
            $adicionales->id_departamento = $request->departamento;
            $adicionales->id_provincia = $request->provincia;
            $adicionales->id_distrito = $request->distrito;
            $adicionales->domicilio = $request->domicilio != null ? ucwords($request->domicilio) : null;
            $adicionales->profile_option = $request->profile_option;

            if ($request->profile_img && $request->hasFile('profile_img')) {
                $request->validate([
                    'profile_img' => 'image|max:1024', // 1MB Max
                    // 'image' => 'file|size:512';
                ]);
                if (!$request->profile_img->isValid()) {
                    throw new Exception("Imagen inválida.");
                } else {
                    $filename = uniqid() . '_' . time() . '.' . $request->profile_img->getClientOriginalExtension();
                    $request->profile_img->move(public_path('images/clientes'), $filename);
                }
            }

            if (isset($filename)) {
                $adicionales->profile_img = $filename;
            }
            $adicionales->save();

            if (!$cliente->adicionales) {
                HistorialCoin::create([
                    'id_cliente' => $cliente->id,
                    'rockocoins' => config('global.rockocoins_adicionales'),
                    'accion' => config('global.action_add_rockocoins'),
                    'descripcion' => 'Adicionales'
                ]);
                $cliente->rockocoins += config('global.rockocoins_adicionales');
                $cliente->adicionales = true;
                $cliente->save();
            }

            DB::commit();
            return 200;
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return 100;
        }
    }

    public function actualizarBasicos(Request $request)
    {
        try {
            $request->validate([
                'uuid' => 'required',
            ]);

            $cliente = Cliente::whereUuid($request->uuid)->first();
            if ($cliente === null) { // user doesn't exist
                return 100;
            }

            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                // 'tipo_documento' => 'required',
                'documento' => 'required|unique:clientes,documento,' . $cliente->id,
                // 'documento' => [
                //     'required',
                //     Rule::unique('clientes')->ignore($cliente->id)->where(function ($query) use ($request) {
                //         return $query->where('tipo_documento', $request->tipo_documento);
                //     })
                // ],
                'genero' => 'required',
                'celular' => 'required',
                'profile_option' => 'required'
            ]);

            // if ($request->tipo_documento == 1) {
            //     if (strlen($request->documento) != 8) {
            //         throw new Exception("Número de documento inválido.");
            //     }
            // } else if ($request->tipo_documento == 2) {
            //     if (strlen($request->documento) != 12) {
            //         throw new Exception("Número de documento inválido.");
            //     }
            // }

            if (ceil(log10($request->celular)) > 15 || ceil(log10($request->documento)) > 20) {
                return 100;
            }
            // Log::error($request->all());

            DB::beginTransaction();
            $cliente->nombre = ucwords($request->nombre);
            $cliente->apellido = ucwords($request->apellido);
            // $cliente->tipo_documento = $request->tipo_documento;
            $cliente->documento = $request->documento;
            $cliente->genero = $request->genero;
            $cliente->celular = $request->celular;
            if ($request->clave != '') {
                $cliente->clave = sha1($request->clave);
            }
            $cliente->save();

            $adicionales = AdicionalesCliente::where('id_cliente', $cliente->id)->first();
            if ($adicionales === null) {
                return 100;
            }

            if ($request->profile_img && $request->hasFile('profile_img')) {
                $request->validate([
                    'profile_img' => 'image'
                    // 'image' => 'file|size:512';
                ]);
                if (!$request->profile_img->isValid()) {
                    throw new Exception("Imagen invalida.");
                } else {
                    $filename = uniqid() . '_' . time() . '.' . $request->profile_img->getClientOriginalExtension();
                    $request->profile_img->move(public_path('images/clientes'), $filename);
                }
            }

            if (isset($filename)) {
                $adicionales->profile_img = $filename;
            }
            $adicionales->profile_option = $request->profile_option;

            $adicionales->save();
            DB::commit();
            return 200;
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return 100;
        }
    }

    public function activateAccount($uuid)
    {
        $cuenta = Cliente::where('uuid', $uuid)->first();
        if ($cuenta == null) {
            return 100;
        }
        $cuenta->verificacion_correo = 2;
        $cuenta->save();

        return 200;
    }

    public function getUsuario($uuid)
    {
        return Cliente::where('uuid', $uuid)->with(['mascotasdata', 'adicionalesdata'])->first();
    }

    public function actualizarAdicionales(Request $request)
    {
        try {
            $request->validate([
                'uuid' => 'required',
                'cumpleanos' => 'required',
                'nacimiento' => 'required',
                'pais' => 'required'
            ]);
            if (!$this->getAge($request->cumpleanos)) {
                throw new Exception('Debes ser mayor de edad.', 100);
            }
            $cliente = Cliente::where('uuid', $request->uuid)->first();
            $adicionales = AdicionalesCliente::where('id_cliente', $cliente->id)->first();
            $adicionales->id_profesion = $request->id_profesion;
            $adicionales->profesion = $request->profesion != null ? ucwords($request->profesion) : null;
            $adicionales->centro_laboral = $request->centro_laboral != null ? ucwords($request->centro_laboral) : null;
            $adicionales->estado_civil = $request->estado_civil;
            $adicionales->cumpleanos = $request->cumpleanos;
            $adicionales->pais_nacimiento = $request->nacimiento;
            $adicionales->pais_residencia = $request->pais;
            $adicionales->id_departamento = $request->departamento;
            $adicionales->id_provincia = $request->provincia;
            $adicionales->id_distrito = $request->distrito;
            $adicionales->domicilio = $request->domicilio != null ? ucwords($request->domicilio) : null;
            $adicionales->save();

            if (!$cliente->adicionales) {
                HistorialCoin::create([
                    'id_cliente' => $cliente->id,
                    'rockocoins' => config('global.rockocoins_adicionales'),
                    'accion' => config('global.action_add_rockocoins'),
                    'descripcion' => 'Adicionales'
                ]);
                $cliente->rockocoins += config('global.rockocoins_adicionales');
                $cliente->adicionales = true;
                $cliente->save();
            }

            return response()->json(['status' => true, 'rockocoins' => $cliente->rockocoins], 200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    public function getPerfil(Request $request)
    {
        $cliente = Cliente::where('uuid', $request->uuid)->with(['mascotasdata', 'adicionalesdata'])->first();
        return $cliente;
    }

    public function validarCorreo($correo, $uuid = null)
    {
        if ($uuid == null) {
            $val = Cliente::where('correo', $correo)->count();
        } else {
            $val = Cliente::where('correo', $correo)->where('uuid', '!=', $uuid)->count();
        }

        if ($val > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updateEmail(Request $request)
    {
        $usuario = Cliente::where('correo', $request->correoActual)->first();
        $usuario->verificacion_correo = 1;
        $usuario->correo = $request->correoNuevo;
        $usuario->save();


        $url = config('global.front_url') . "/validar-correo/" . $usuario->uuid;
        if (Mail::to($request->correoNuevo)->send(new ValidarEmail($url))) {
            return 200;
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json(['status' => true, 'msj' => 'Se cerró correctamente.'], 200);
    }
}
