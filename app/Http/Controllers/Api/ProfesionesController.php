<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Profesion;
use Illuminate\Http\Request;

class ProfesionesController extends Controller
{
    public function getProfesiones()
    {
        return Profesion::where('id','!=','1')->orderBy('name','asc')->get();
    }
}
