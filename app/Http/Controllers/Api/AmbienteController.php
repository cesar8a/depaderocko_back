<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Propiedad;
use App\Models\TipoAmbiente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class AmbienteController extends Controller
{
    function getTipoAmbientes(Request $request)
    {
        $tipo_ambientes = [];
        if ($request->id_propiedad == null) {
            $tipo_ambientes = TipoAmbiente::all();
        } else {
            $tipo_ambientes = TipoAmbiente::withCount(['ambientes_data as count' => function (Builder $query) use ($request) {
                $query->where('propiedad_id', $request->id_propiedad);
            }])->orderBy('id')->get();
        }
        return response()->json(['status' => true, 'tipo_ambientes' => $tipo_ambientes], 200);
    }

    function getAmbientesPropiedad(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $ambientes = $propiedad->ambientes_data()->orderBy('id')->get();
            return response()->json(['status' => true, 'ambientes' => $ambientes], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function agregarAmbientesPropiedad(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_propiedad' => 'required',
                'ambientes' => 'required|array',
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->with('ambientes_data')->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            foreach ($request->ambientes as $value) {
                if ($value['count'] != 0) {
                    $ambientes = $propiedad::where('id', $request->id_propiedad)->withCount(['ambientes_data' => function (Builder $query) use ($request, $value) {
                        $query->where('tipo_ambiente_id', $value['id']);
                    }])->first();
                    Log::info($ambientes);
                    $ambientes_data_count = $ambientes->ambientes_data_count;
                    Log::info($ambientes_data_count);

                    if ($value['count'] > $ambientes_data_count) {
                        for ($i = 0; $i < ($value['count'] - $ambientes_data_count); $i++) {
                            $propiedad->ambientes_data()->attach($value['id']); // Fachada
                        }
                    }
                }
            }
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getAmbienteData(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
                'id_ambiente' => 'required'
            ]);
            // $ambiente = Propiedad::with('ambientes_data')->get()->ambientes_data()->having('pivot_id', $request->id_ambiente)->first();
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $ambiente = $propiedad->ambientes_data()->wherePivot('id', $request->id_ambiente)->first();
            if ($ambiente == null) {
                throw new Exception('No se encontraron los datos');
            }
            return response()->json(['status' => true, 'ambiente' => $ambiente], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarAmbienteData(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_propiedad' => 'required',
                'id_ambiente' => 'required',
                'descripcion' => 'required',
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $ambiente = $propiedad->ambientes_data()->wherePivot('id', $request->id_ambiente)->first();
            Log::info($ambiente);
            $ambiente->pivot->descripcion = $request->descripcion;

            if ($request->imagen1_url && $request->hasFile('imagen1_url')) {
                $request->validate([
                    'imagen1_url' => 'image|max:1024', // 1MB Max
                ]);
                if (!$request->imagen1_url->isValid()) {
                    throw new Exception("Imagen inválida.");
                } else {
                    $filename = uniqid() . '_' . time() . '.' . $request->imagen1_url->getClientOriginalExtension();
                    $request->imagen1_url->move(public_path('images/propiedades/ambientes'), $filename);
                    $ambiente->pivot->imagen1_url = $filename;
                }
            }

            if ($request->imagen2_url && $request->hasFile('imagen2_url')) {
                $request->validate([
                    'imagen2_url' => 'image|max:1024', // 1MB Max
                ]);
                if (!$request->imagen2_url->isValid()) {
                    throw new Exception("Imagen inválida.");
                } else {
                    $filename = uniqid() . '_' . time() . '.' . $request->imagen2_url->getClientOriginalExtension();
                    $request->imagen2_url->move(public_path('images/propiedades/ambientes'), $filename);
                    $ambiente->pivot->imagen2_url = $filename;
                }
            }
            $ambiente->pivot->save();

            if (
                $propiedad->completo_disponibilidad == 0 && $ambiente->pivot->tipo_ambiente_id == 1
                && $ambiente->pivot->imagen1_url != null && $ambiente->pivot->imagen2_url != null
            ) {
                $propiedad->completo_disponibilidad = 1;
                $propiedad->save();
            }
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
        // $managementUnit->councils()->where('id', 1)->wherePivot('year', 2011)->detach(1);
        // return response()->json(['status' => true, 'ambiente' => $ambiente], 200);
    }

    function eliminarAmbienteData(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_propiedad' => 'required',
                'id_ambiente' => 'required'
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            $propiedad->ambientes_data()->wherePivot('id', $request->id_ambiente)->detach();
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[AmbienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
