<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Propiedad;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class VideoController extends Controller
{
    function getVideosPropiedad(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            $videos = Video::where('id_propiedad', $request->id_propiedad)->get();
            return response()->json(['status' => true, 'videos' => $videos], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function agregarVideoPropiedad(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_propiedad' => 'required',
                'video'  => 'required|mimes:mp4,mov,ogg,qt|max:4000'
            ]);

            if ($request->video && $request->hasFile('video')) {
                if (!$request->video->isValid()) {
                    throw new Exception("Video inválido.");
                } else {
                    $getID3 = new \getID3;
                    $file = $getID3->analyze($request->video);
                    $duration = $file['playtime_seconds'];
                    if ($duration > 30) {
                        throw new Exception('El video solo puede durar 30 segundos.');
                    }
                    $filename = uniqid() . '_' . time() . '.' . $request->video->getClientOriginalExtension();
                    $request->video->move(public_path('videos/propiedades'), $filename);
                    $video = Video::create([
                        'id_propiedad' => $request->id_propiedad,
                        'video_url' => $filename
                    ]);
                    if ($video->propiedad->completo_video == 0) {
                        $propiedad = Propiedad::findOrFail($video->id_propiedad);
                        $propiedad->completo_video = 1;
                        $propiedad->save();
                    }
                }
            }
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function editVideoPropiedad(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_video' => 'required',
                'video'  => 'required|mimes:mp4,mov,ogg,qt|max:4000'
            ]);
            $video = Video::where('id', $request->id_video)->first();
            if ($video == null) {
                throw new Exception('No se encontraron los datos');
            }

            if ($request->video && $request->hasFile('video')) {
                if (!$request->video->isValid()) {
                    throw new Exception("Video inválido.");
                } else {
                    $getID3 = new \getID3;
                    $file = $getID3->analyze($request->video);
                    $duration = $file['playtime_seconds'];
                    if ($duration > 30) {
                        throw new Exception('El video solo puede durar 30 segundos.');
                    }
                    $filename = uniqid() . '_' . time() . '.' . $request->video->getClientOriginalExtension();
                    $current_url = $video->video_url;
                    $request->video->move(public_path('videos/propiedades'), $filename);
                    $video->video_url = $filename;
                    $video->save();
                    unlink(public_path('videos/propiedades') . '/' . $current_url);
                }
            }
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function eliminarVideoPropiedad(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_video' => 'required'
            ]);
            $video = Video::where('id', $request->id_video)->first();
            if ($video == null) {
                throw new Exception('No se encontraron los datos');
            }
            $propiedad = Propiedad::where('id', $video->id_propiedad)->withCount('videos_data')->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            if ($propiedad->videos_data_count == 1) {
                $propiedad->completo_video = false;
                $propiedad->save();
            }
            unlink(public_path('videos/propiedades') . '/' . $video->video_url);
            $video->delete();

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ServicioController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
