<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Contacto;
use App\Models\Informacion;
use App\Models\Propiedad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class PropiedadController extends Controller
{
    public function createPropiedad(Request $request)
    {
        try {
            DB::beginTransaction();

            $request->validate([
                'nombre' => 'required|unique:propiedades',
            ]);

            $propiedad = Propiedad::create([
                'id_cliente' => $request->user()->id,
                'nombre' => $request->nombre
            ]);

            // Tablas de los modulos

            Contacto::create([
                'id_propiedad' => $propiedad->id
            ]);

            Informacion::create([
                'id_propiedad' => $propiedad->id
            ]);

            $propiedad->ambientes_data()->attach(1); // Fachada

            DB::commit();
            return response()->json(['status' => true, 'created_id' => $propiedad->id], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    public function editNombrePropiedad(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            $propiedad = Propiedad::where('id', $request->id_propiedad)->where('active', true)->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }

            $request->validate([
                'nombre' => 'required|unique:propiedades,nombre,' . $propiedad->id,
            ]);
            $propiedad->nombre = $request->nombre;
            $propiedad->save();

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getPropiedades(Request $request)
    {
        try {
            $propiedades = Propiedad::where('id_cliente', $request->user()->id)->select('id', 'nombre', 'status')
                ->where('active', true)
                ->withCount('videos_data')->get();
            return response()->json(['status' => true, 'propiedades' => $propiedades], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getModulos(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            // $propiedad = Propiedad::findOrFail($request->id_propiedad);
            $propiedad = Propiedad::where('id', $request->id_propiedad)
                ->select('id', 'nombre', 'status', 'completo_contacto', 'completo_informacion', 'completo_ambiente', 'completo_video', 'completo_disponibilidad')
                ->first();
            if ($propiedad == null) {
                throw new Exception('No se encontraron los datos');
            }
            return response()->json(['status' => true, 'modulos' => $propiedad], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function eliminarPropiedad(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            DB::beginTransaction();

            $propiedad = Propiedad::findOrFail($request->id_propiedad);
            $propiedad->active = false;
            $propiedad->save();

            DB::commit();

            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function getCercanosTest(Request $request)
    {
        try {
            $request->validate([
                'latitude' => 'required',
                'longitude' => 'required',
            ]);
            $propiedades = Propiedad::whereBetween('maps_latitude', [$request->latitude - 0.01, $request->latitude + 0.01])
                ->whereBetween('maps_longitude', [$request->longitude - 0.01, $request->longitude + 0.01])

                ->get();
            return response()->json(['status' => true, 'propiedades' => $propiedades], 200);
        } catch (\Exception $e) {
            Log::error('[PropiedadController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
