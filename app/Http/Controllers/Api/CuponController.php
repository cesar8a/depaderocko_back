<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\{Cupon, Cliente, HistorialCoin};
use Illuminate\Http\Request;
use DB;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Mail\CanjeoMail;
use Illuminate\Support\Facades\Mail;

class CuponController extends Controller
{
    //
    public function reclamarCupon(Request $request)
    {

        try {
            DB::beginTransaction();

            $request->validate([
                'uuid' => 'required',
                'codigo' => 'required'
            ]);

            $cliente = Cliente::where('uuid', $request->uuid)->first();
            if ($cliente === null) {
                throw new Exception('El cliente no existe.',100);
            }
            $now = \Carbon\Carbon::now()->toDateTimeString();
            $cupon = Cupon::where('codigo', 'like', $request->codigo)->first();
            if ($cupon === null) {
                throw new Exception('Este cupón no existe.',103);
            }
            $cupon = Cupon::where('codigo', 'like', $request->codigo)
                ->where('fec_fin', '>=', $now)
                ->where('fec_inicio', '<=', $now)
                ->first();
            if ($cupon === null) {
                throw new Exception('Este cupón no está activo.',101);
            }
            $historial = HistorialCoin::where('id_cliente', $cliente->id)->where('id_cupon', $cupon->id)->first();
            if ($historial != null) {
                throw new Exception('El usuario ya reclamó este cupón anteriormente.',102);
            }

            HistorialCoin::create([
                'id_cliente' => $cliente->id,
                'rockocoins' => $cupon->rockocoins,
                'accion' => config('global.action_add_rockocoins'),
                'descripcion' => 'Reclamo de cupón',
                'id_cupon' => $cupon->id,
                'codigo_cupon' => $cupon->codigo,
            ]);

            $cliente->rockocoins = ($cliente->rockocoins + $cupon->rockocoins);
            $cliente->save();
            $cupon->increment('count');
            Mail::to($cliente->correo)->send((new CanjeoMail(2,$cupon->rockocoins))->afterCommit());
            DB::commit();
            return response()->json(['status' => true, 'rockocoins_cupon' => $cupon->rockocoins, 'rockocoins' => $cliente->rockocoins], 200);
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
