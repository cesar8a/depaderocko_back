<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Contacto;
use App\Models\Propiedad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class ContactoController extends Controller
{
    function getContacto(Request $request)
    {
        try {
            $request->validate([
                'id_propiedad' => 'required',
            ]);
            $contacto = Contacto::where('id_propiedad', $request->id_propiedad)->first();
            if ($contacto == null) {
                throw new Exception('No se encontraron los datos');
            }
            return response()->json(['status' => true, 'contacto' => $contacto], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarEmpresa(Request $request)
    {
        try {
            $request->validate([
                'id_contacto' => 'required',
            ]);
            $contacto = Contacto::findOrFail($request->id_contacto);
            $request->validate([
                'tipo_persona' => 'required',
                'razon_social' => 'required|unique:contactos,razon_social,' . $contacto->id,
                'ruc' => 'required|unique:contactos,ruc,' . $contacto->id,
                'giro' => 'required',
                'id_pais' => 'required',
                'direccion' => 'required',
                'check_terminos_empresa' => 'required'
            ]);
            DB::beginTransaction();
            if ($request->check_terminos_empresa == 0) {
                throw new Exception('Debe aceptar los terminos y condiciones.');
            }

            $contacto->tipo_persona = config('global.tipo_persona_empresa');
            $contacto->razon_social = $request->razon_social;
            $contacto->ruc = $request->ruc;
            $contacto->giro = $request->giro;
            $contacto->id_pais = $request->id_pais;
            $contacto->id_departamento = $request->id_departamento;
            $contacto->id_provincia = $request->id_provincia;
            $contacto->id_distrito = $request->id_distrito;
            $contacto->direccion = $request->direccion;
            $contacto->check_terminos_empresa = $request->check_terminos_empresa;
            $contacto->save();

            if ($contacto->propiedad->completo_contacto == 0) {
                if ($contacto->correo != null && $contacto->celular != null) {
                    $propiedad = Propiedad::findOrFail($contacto->id_propiedad);
                    $propiedad->completo_contacto = 1;
                    $propiedad->save();
                }
            }

            DB::commit();

            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }

    function guardarContacto(Request $request)
    {
        try {
            $request->validate([
                'id_contacto' => 'required',
                'tipo_persona' => 'required',
                'correo' => 'required',
                'celular' => 'required',
                'check_terminos' => 'required',
                'check_datos' => 'required',
                // 'pagina_web' => 'url',
            ]);
            DB::beginTransaction();
            if ($request->check_terminos == 0) {
                throw new Exception('Debe aceptar los terminos y condiciones.');
            }

            $contacto = Contacto::findOrFail($request->id_contacto);
            if ($request->tipo_persona == config('global.tipo_persona_natural')) {
                $contacto->razon_social = null;
                $contacto->ruc = null;
                $contacto->giro = null;
                $contacto->id_pais = null;
                $contacto->id_departamento = null;
                $contacto->id_provincia = null;
                $contacto->id_distrito = null;
                $contacto->direccion = null;
                $contacto->check_terminos_empresa = 0; // Terminos solo para Tipo persona juridica
            }
            $contacto->tipo_persona = $request->tipo_persona;
            $contacto->correo = $request->correo;
            $contacto->celular = $request->celular;
            $contacto->pagina_web = $request->pagina_web;
            $contacto->linkedin = $request->linkedin;
            $contacto->instagram = $request->instagram;
            $contacto->tiktok = $request->tiktok;
            $contacto->facebook = $request->facebook;
            $contacto->whatsapp = $request->whatsapp;
            $contacto->check_terminos = $request->check_terminos;
            $contacto->check_datos = $request->check_datos;
            $contacto->save();

            if ($contacto->propiedad->completo_contacto == 0) {
                if ($request->tipo_persona == config('global.tipo_persona_natural')) {
                    $propiedad = Propiedad::findOrFail($contacto->id_propiedad);
                    $propiedad->completo_contacto = 1;
                    $propiedad->save();
                } else {
                    if (
                        $contacto->razon_social != null && $contacto->ruc != null && $contacto->giro != null &&
                        $contacto->id_pais != null && $contacto->direccion != null && $contacto->check_terminos_empresa != 0
                    ) {
                        $propiedad = Propiedad::findOrFail($contacto->id_propiedad);
                        $propiedad->completo_contacto = 1;
                        $propiedad->save();
                    }
                }
            }
            DB::commit();

            return response()->json(['status' => true], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => 900], 200);
        } catch (\Exception $e) {
            Log::error('[ContactoController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()], 200);
        }
    }
}
