<?php

namespace App\Http\Controllers;

use DB;
use App\Mail\NuevoContacto;
use App\Mail\NuevoReclamo;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class EmailController extends Controller
{
    public function contacto(Request $request)
    {
        try {
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'correo' => 'required',
                'asunto' => 'required',
                'mensaje' => 'required'
            ]);
            Mail::to('contacto@eldepaderocko.com')->send(new NuevoContacto($request));
            return 200;
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            // DB::rollback();
            return 100;
        }
    }

    public function reclamacion(Request $request)
    {
        try {
            $request->validate([
                'tipo_persona' => 'required|not-in:0',
                'nombre' => 'required_if:tipo_persona,1',
                'apellido' => 'required_if:tipo_persona,1',
                'tipo_documento' => 'required_if:tipo_persona,1',
                'documento' => 'required_if:tipo_persona,1',
                'razon_social' => 'required_if:tipo_persona,2',
                'ruc' => 'required_if:tipo_persona,2',
                'correo' => 'required',
                'telefono' => 'required',
                'domicilio' => 'required',
                'tipo' => 'required',
                'monto' => 'required',
                'tipo_accion' => 'required',
                // 'detalle' => 'required',
                // 'pedido' => 'required'
            ]);
            Mail::to('librodereclamaciones@eldepaderocko.com')->send(new NuevoContacto($request));
            return 200;
        } catch (\Exception $e) {
            Log::error('[ClienteController] ' . $e->getMessage() . '. ' . $e->getFile() . ':' . $e->getLine());
            // DB::rollback();
            return 100;
        }
    }
}
