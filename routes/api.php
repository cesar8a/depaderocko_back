<?php

// namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\AmbienteController;
use App\Http\Controllers\CategoriaPostController;
use App\Http\Controllers\Api\ClienteController;
use App\Http\Controllers\Api\ContactoController;
use App\Http\Controllers\Api\CuponController;
use App\Http\Controllers\Api\InformacionController;
use App\Http\Controllers\Api\ProfesionesController;
use App\Http\Controllers\Api\PropiedadController;
use App\Http\Controllers\Api\ServicioController;
use App\Http\Controllers\Api\VideoController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\MacostaController;
use App\Http\Controllers\PaisController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('posts', [PostController::class, 'getAll']);
Route::get('categorias', [CategoriaPostController::class, 'getAll']);
// Route::get('admins', [UserController::class, 'getAll']); // Eliminar de front
Route::get('post/{slug}', [PostController::class, 'getPost']);
Route::post('searchPosts', [PostController::class, 'searchPosts']);
Route::post('checkLike', [PostController::class, 'checkLike']);

//Emails
Route::post('/email/contacto', [EmailController::class, 'contacto']);
Route::post('/email/reclamacion', [EmailController::class, 'reclamacion']);

//Fase 2
Route::post('/registrer', [ClienteController::class, 'registrer']);
Route::get('activateAccount/{uuid}', [ClienteController::class, 'activateAccount']);
Route::get('validarCorreo/{correo}/{uuid?}', [ClienteController::class, 'validarCorreo']);
Route::post('login', [ClienteController::class, 'login']);
Route::post('asignarClave', [ClienteController::class, 'asignarClave']);
// Route::get('eliminar', [ClienteController::class, 'eliminar']);


//Direcciones
Route::get('getPaises', [PaisController::class, 'getPaises']);
Route::get('getDepartamentos', [PaisController::class, 'getDepartamentos']);
Route::get('getProvincias/{idDepartamento}', [PaisController::class, 'getProvincias']);
Route::get('getDistritos/{idProvincia}', [PaisController::class, 'getDistrito']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('reset-account/{correo}', [ClienteController::class, 'resetAccount']);
    Route::post('logout', [ClienteController::class, 'logout']);

    Route::post('likePost', [PostController::class, 'likePost']);
    Route::post('unlikePost', [PostController::class, 'unlikePost']);

    //Cupones
    Route::post('reclamarCupon', [CuponController::class, 'reclamarCupon']);
    Route::get('getProfesiones', [ProfesionesController::class, 'getProfesiones']);

    /* Pantalla verificación Inicio */
    Route::get('getUsuario/{uuid}', [ClienteController::class, 'getUsuario']);
    Route::post('storeAdicionales', [ClienteController::class, 'storeAdicionales']);
    /* Pantalla verificación Fin */

    /* Pantalla perfil Inicio */
    Route::post('getPerfil', [ClienteController::class, 'getPerfil']);
    Route::post('update-basicios', [ClienteController::class, 'actualizarBasicos']);
    Route::post('updateEmail', [ClienteController::class, 'updateEmail']);
    Route::post('update-adicionales', [ClienteController::class, 'actualizarAdicionales']);
    /* Pantalla perfil Fin */

    Route::post('storeMascota', [MacostaController::class, 'store']);
    Route::get('getMascotas/{uuid}', [MacostaController::class, 'getMascotas']);
    Route::post('eliminarMascota', [MacostaController::class, 'eliminar']);

    Route::post('getPropiedades', [PropiedadController::class, 'getPropiedades']);
    Route::post('createPropiedad', [PropiedadController::class, 'createPropiedad']);
    Route::post('eliminarPropiedad', [PropiedadController::class, 'eliminarPropiedad']);
    Route::post('editNombrePropiedad', [PropiedadController::class, 'editNombrePropiedad']);
    Route::post('cambiarImagenPropiedad', [PropiedadController::class, 'cambiarImagenPropiedad']);
    
    Route::post('getModulos', [PropiedadController::class, 'getModulos']);

    Route::post('getContacto', [ContactoController::class, 'getContacto']);
    Route::post('guardarEmpresa', [ContactoController::class, 'guardarEmpresa']);
    Route::post('guardarContacto', [ContactoController::class, 'guardarContacto']);

    Route::post('getInformacion', [InformacionController::class, 'getInformacion']);
    Route::post('guardarInformacion', [InformacionController::class, 'guardarInformacion']);
    Route::post('guardarUbicacion', [InformacionController::class, 'guardarUbicacion']);
    Route::post('getTipoPropiedades', [InformacionController::class, 'getTipoPropiedades']);
    Route::post('getTipoDepas', [InformacionController::class, 'getTipoDepas']);

    Route::post('guardarServicios', [ServicioController::class, 'guardarServicios']);
    Route::post('getServiciosPropiedad', [ServicioController::class, 'getServiciosPropiedad']);
    Route::post('getTipoServicios', [ServicioController::class, 'getTipoServicios']);
    
    Route::post('getAmbientesPropiedad', [AmbienteController::class, 'getAmbientesPropiedad']);
    Route::post('getTipoAmbientes', [AmbienteController::class, 'getTipoAmbientes']);
    Route::post('agregarAmbientesPropiedad', [AmbienteController::class, 'agregarAmbientesPropiedad']);
    Route::post('getAmbienteData', [AmbienteController::class, 'getAmbienteData']);
    Route::post('guardarAmbienteData', [AmbienteController::class, 'guardarAmbienteData']);
    Route::post('eliminarAmbienteData', [AmbienteController::class, 'eliminarAmbienteData']);
    
    Route::post('getVideosPropiedad', [VideoController::class, 'getVideosPropiedad']);
    Route::post('agregarVideoPropiedad', [VideoController::class, 'agregarVideoPropiedad']);
    Route::post('editVideoPropiedad', [VideoController::class, 'editVideoPropiedad']);
    Route::post('eliminarVideoPropiedad', [VideoController::class, 'eliminarVideoPropiedad']);
    
    
    
    // Test de búsqueda
    Route::post('getCercanosTest', [PropiedadController::class, 'getCercanosTest']);
});
