<?php
namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    if (isset(Auth::user()->id)) {
        return redirect('home');
    } else {
        return view('welcome');
    }
})->name('index');



Route::post('login', [UserController::class, 'login'])->name('login');


Route::middleware(['auth'])->group(function () {
    Route::get('home', [HomeController::class, 'home'])->name('home');
    Route::get('logout', [UserController::class, 'logout'])->name('logout');
    Route::get('categorias', [CategoriaPostController::class, 'index'])->name('categorias');
    
    Route::get('posts', [PostController::class, 'index'])->name('posts');
    Route::get('createPost', [PostController::class, 'createPost'])->name('createPost');
    Route::get('editarPost/{slug}', [PostController::class, 'editPost'])->name('editPost');
    Route::post('storePost', [PostController::class, 'store'])->name('storePost');
    Route::post('updatePost', [PostController::class, 'update'])->name('updatePost');
    
    Route::any('clientes', [ClienteController::class, 'index'])->name('clientes');
    Route::any('cupones', [CuponController::class, 'index'])->name('cupones');
    Route::any('rockodata', [RockoDataController::class, 'index'])->name('rockodata');
    Route::any('kpi', [KPIController::class, 'index'])->name('kpi');

    

});