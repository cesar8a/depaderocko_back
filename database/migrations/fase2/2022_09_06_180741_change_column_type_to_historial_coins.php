<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historial_coins', function (Blueprint $table) {
            $table->unsignedBigInteger('id_cliente')->nullable()->change();
            $table->unsignedBigInteger('id_cupon')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historial_coins', function (Blueprint $table) {
            $table->integer('id_cliente')->change();
            $table->integer('id_cupon')->nullable()->change();
        });
    }
};
