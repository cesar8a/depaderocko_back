<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('macostas', function (Blueprint $table) {
            $table->integer('sexo')->nullable();
            $table->string('raza')->nullable();
            $table->string('hobbie')->nullable();
            $table->string('foto')->nullable();
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->string('foto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
