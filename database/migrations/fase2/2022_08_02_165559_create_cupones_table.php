<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupones', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 50);
            $table->string('codigo', 50)->unique();
            $table->integer('rockocoins');
            $table->text('descripcion');
            $table->bigInteger('count')->default(0);
            $table->timestamp('fec_inicio', 0)->nullable();
            $table->timestamp('fec_fin', 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupones');
    }
};