<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adicionales_clientes', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cliente');
            $table->string('profesion',200)->nullable();
            $table->integer('estado_civil')->nullable();
            $table->date('cumpleanos');
            $table->integer('pais_nacimiento');
            $table->integer('pais_residencia');
            $table->integer('id_departamento')->nullable();
            $table->integer('id_provincia')->nullable();
            $table->integer('id_distrito')->nullable();
            $table->string('domicilio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adicionales_clientes');
    }
};
