<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->string('id_departamento',10)->nullable()->change();
            $table->string('id_provincia',10)->nullable()->change();
            $table->string('id_distrito',10)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->integer('id_departamento')->nullable()->change();
            $table->integer('id_provincia')->nullable()->change();
            $table->integer('id_distrito')->nullable()->change();
            //
        });
    }
};
