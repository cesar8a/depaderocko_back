<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->unsignedBigInteger('id_cliente')->nullable()->change();
            $table->unsignedBigInteger('pais_nacimiento')->nullable()->change();
            $table->unsignedBigInteger('pais_residencia')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->integer('id_cliente')->nullable()->change();
            $table->integer('pais_nacimiento')->nullable()->change();
            $table->integer('pais_residencia')->nullable()->change();
        });
    }
};
