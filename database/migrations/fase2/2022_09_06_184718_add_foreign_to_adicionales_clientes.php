<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
            $table->foreign('pais_nacimiento')->references('id')->on('pais')->onDelete('cascade');
            $table->foreign('pais_residencia')->references('id')->on('pais')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->dropForeign(['id_cliente']);
            $table->dropForeign(['pais_nacimiento']);
            $table->dropForeign(['pais_residencia']);
        });
    }
};
