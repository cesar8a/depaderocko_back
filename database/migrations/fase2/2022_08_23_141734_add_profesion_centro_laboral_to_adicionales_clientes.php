<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->unsignedBigInteger('id_profesion')->nullable();
            $table->string('centro_laboral')->nullable();
            $table->foreign('id_profesion')->references('id')->on('profesiones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicionales_clientes', function (Blueprint $table) {
            $table->dropForeign('adicionales_clientes_id_profesion_foreign');
            $table->dropColumn(['id_profesion','centro_laboral']);
        });
    }
};
