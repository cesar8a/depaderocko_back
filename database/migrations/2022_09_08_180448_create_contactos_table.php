<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_propiedad');
            $table->unsignedBigInteger('tipo_persona')->nullable();
            $table->string('razon_social')->nullable();
            $table->string('ruc')->nullable();
            $table->string('giro')->nullable();
            $table->unsignedBigInteger('id_pais')->nullable();
            $table->string('id_departamento',2)->nullable();
            $table->string('id_provincia',4)->nullable();
            $table->string('id_distrito',6)->nullable();
            $table->string('direccion')->nullable();
            $table->boolean('check_terminos_empresa')->nullable()->default(0);

            $table->string('correo')->nullable();
            $table->string('celular')->nullable();
            $table->string('pagina_web')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('tiktok')->nullable();
            $table->string('facebook')->nullable();
            $table->string('whatsapp')->nullable();
            $table->boolean('check_terminos')->nullable()->default(0);
            $table->boolean('check_datos')->nullable()->default(0);

            $table->foreign('id_propiedad')->references('id')->on('propiedades')->onDelete('cascade');
            $table->foreign('id_pais')->references('id')->on('pais')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
};
