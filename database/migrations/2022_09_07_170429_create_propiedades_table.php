<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_cliente');
            $table->string('nombre',100)->unique();
            // $table->string('imagen_url')->nullable();
            $table->string('direccion')->nullable();
            // $table->string('referencia')->nullable();
            $table->string('maps_direccion')->nullable();
            $table->decimal('maps_latitude',12,8)->nullable();
            $table->decimal('maps_longitude',12,8)->nullable();
            // $table->point('maps_point', 0);
            // $table->spatialIndex('maps_point');

            $table->boolean('completo_contacto')->nullable()->default(0);
            $table->boolean('completo_informacion')->nullable()->default(0);
            $table->boolean('completo_ambiente')->nullable()->default(0);
            $table->boolean('completo_video')->nullable()->default(0);
            $table->boolean('completo_disponibilidad')->nullable()->default(0);

            $table->unsignedSmallInteger('status')->nullable()->default(1);
            $table->boolean('active')->nullable()->default(1);
            $table->foreign('id_cliente')->references('id')->on('clientes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedades');
    }
};
