<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_propiedad');
            $table->unsignedBigInteger('id_tipo_propiedad')->nullable();
            $table->unsignedBigInteger('id_tipo_depa')->nullable();
            $table->boolean('vista_calle')->nullable();
            $table->integer('old_years')->nullable();
            $table->integer('pisos')->nullable();
            $table->integer('area_total')->nullable();
            $table->boolean('ascensor')->nullable();
            $table->boolean('directo')->nullable();
            $table->unsignedBigInteger('id_pais')->nullable();
            $table->string('id_departamento',2)->nullable();
            $table->string('id_provincia',4)->nullable();
            $table->string('id_distrito',6)->nullable();
            $table->string('referencia')->nullable();
            
            $table->foreign('id_propiedad')->references('id')->on('propiedades')->onDelete('cascade');
            $table->foreign('id_tipo_propiedad')->references('id')->on('tipo_propiedades')->onDelete('cascade');
            $table->foreign('id_tipo_depa')->references('id')->on('tipo_depas')->onDelete('cascade');
            $table->foreign('id_pais')->references('id')->on('pais')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informaciones');
    }
};
