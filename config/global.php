<?php

if (config('app.env') === 'production') {
    $front_url = 'https://eldepaderocko.com';
    $imagen_url = "https://fotos.eldepaderocko.com/images/posts/";
} else if (config('app.env') === 'certi') {
    $front_url = 'https://certifront.eldepaderocko.com';
    $imagen_url = "https://certiback.eldepaderocko.com/images/posts/";
} else {
    $front_url = 'http://localhost:3000';
    $imagen_url = "http://localhost:8000/images/posts/";
}

return [
    'front_url' => $front_url,
    "imagen_url" => $imagen_url,

    'user_type' => ['User', 'Admin'],

    'action_add_rockocoins' => 'AUMENTAR',
    'action_decrease_rockocoins' => 'DISMINUIR',
    'rockocoins_registro' => 30,
    'rockocoins_adicionales' => 30,
    'rockocoins_mascota' => 40,
    'tipo_persona_natural' => 1,
    'tipo_persona_empresa' => 2
]

?>